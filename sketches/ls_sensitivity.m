clear all; close all; clc
addpath(strcat('..',filesep, 'simulations', filesep, 'plotting', filesep, 'util'))

%%

a1 = [1.5;1]; a2 = [1.5;0.75];a1=a1/norm(a1);a2=a2/norm(a2); A = [a1,a2];
% a1_hat = [1;0]; a2_hat = [0;1]; A_hat = [a1_hat,a2_hat];
origin = [0;0];

%%
headsize = 0.7;
color = 1.3*get_RGBColorCodes({'red', 'blue'})';

a1_qui = quiver(origin(1), origin(2), a1(1), a1(2),'Color',color(1,:),'LineWidth',2, 'AutoScale','off','MaxHeadSize',headsize/norm(a1));
hold on
a2_qui = quiver(origin(1), origin(2), a2(1), a2(2),'Color',color(1,:),'LineWidth',2, 'AutoScale','off','MaxHeadSize',headsize/norm(a2));

%%
% a1_hat_qui = quiver(origin(1), origin(2), a1_hat(1), a1_hat(2),'Color',color(2,:),'LineWidth',2, 'AutoScale','off','MaxHeadSize',headsize/norm(a1_hat));
% a2_hat_qui = quiver(origin(1), origin(2), a2_hat(1), a2_hat(2),'Color',color(2,:),'LineWidth',2, 'AutoScale','off','MaxHeadSize',headsize/norm(a2_hat));

%%
b = [1; 0.8];
b_pert = b+[-0.02; 0.07];
x_ls = (A'*A)^-1*A'*b;
x_ls_pert = (A'*A)^-1*A'*b_pert;
[U,S,V] = svd(A);
u1 = -0.4*U(1,:);
u2 = -0.4*U(2,:);
% b_pert
quiver(a1(1), a1(2), (x_ls_pert(1)-1)*a1(1), (x_ls_pert(1)-1)*a1(2),'Color',[0.7,0.7,0.7],'LineWidth',2, 'AutoScale','off','MaxHeadSize',headsize/(norm(x_ls_pert(1))-1));
quiver(x_ls_pert(1)*a1(1), x_ls_pert(1)*a1(2), x_ls_pert(2)*a2(1), x_ls_pert(2)*a2(2),'Color',[0.7,0.7,0.7],'LineWidth',2, 'AutoScale','off','MaxHeadSize',headsize/(norm(x_ls_pert(2))));
% b
quiver(a1(1), a1(2), (x_ls(1)-1)*a1(1), (x_ls(1)-1)*a1(2),'Color','k','LineWidth',2, 'AutoScale','off','MaxHeadSize',headsize/(norm(x_ls(1))-1));
quiver(x_ls(1)*a1(1), x_ls(1)*a1(2), x_ls(2)*a2(1), x_ls(2)*a2(2),'Color','k','LineWidth',2, 'AutoScale','off','MaxHeadSize',headsize/(norm(x_ls(2))));

plot(b(1),b(2),'o','LineWidth',2,'Color','k')
plot(b_pert(1),b_pert(2),'o','LineWidth',2,'Color','k')

quiver(origin(1), origin(2), u1(1), u1(2),'Color','k','LineWidth',2, 'AutoScale','off','MaxHeadSize',headsize/norm(u1));
quiver(origin(1), origin(2), u2(1), u2(2),'Color','k','LineWidth',2, 'AutoScale','off','MaxHeadSize',headsize/norm(u2));

%%
grid on
yl = [0,1.95]-0.38; ylim(yl); xl = [0, 400/280*(yl(2)-yl(1))]; xlim(xl);
xticklabels({''}); yticklabels({''});
xti = 0:0.5:3; xticks(xti);yticks(xti);
setSize([400 280]);
setFontSize([11;11;11;11]);
grid on
legend('off')

%%
fs = 12;
pos_a1 = 0.3*a1+[0.2;0];
text(pos_a1(1),pos_a1(2),'\boldmath$a_1$','Interpreter','latex','FontSize',fs,'Color',a1_qui.Color)
pos_a2 = 0.3*a2+[0;0.3];
text(pos_a2(1),pos_a2(2),'\boldmath$a_2$','Interpreter','latex','FontSize',fs,'Color',a2_qui.Color)
pos_xls = [1.25;0.75];
text(pos_xls(1),pos_xls(2),'{\boldmath$x$}$_{LS,1}$','Interpreter','latex','FontSize',fs,'Color','k')

pos_xls = [1.35;1.35];
text(pos_xls(1),pos_xls(2),'{\boldmath$x$}$_{LS,2}$','Interpreter','latex','FontSize',fs,'Color',0.7*[0.7,0.7,0.7])

pos_b = [0.75;0.75];
text(pos_b(1),pos_b(2),'{\boldmath$\hat{b}$}$_{1}$','Interpreter','latex','FontSize',fs,'Color','k')

pos_b2 = [0.75;0.95];
text(pos_b2(1),pos_b2(2),'{\boldmath$\hat{b}$}$_{2}$','Interpreter','latex','FontSize',fs,'Color','k')

pos_u1 = [0.02;0.2];
text(pos_u1(1),pos_u1(2),'{\boldmath$u$}$_{1}$','Interpreter','latex','FontSize',fs,'Color','k')

pos_u2 = [0.12; -0.1];
text(pos_u2(1),pos_u2(2),'{\boldmath$u$}$_{2}$','Interpreter','latex','FontSize',fs,'Color','k')

%%
set(gca,'XColor', 'none','YColor','none')
box('off')
print('ls-sensitivity-sketch','-depsc')