clear all; close all; clc
addpath(strcat('..',filesep, 'simulations', filesep, 'plotting', filesep, 'util'))

A = [0.1, 0.3, 0.5,0.75 0.8, 0.92, 1.15, 1.3]';
b = [0, 0, 0, 1,0, 1, 0, 1]';

[~, S_bar, ~] = svd([A,b]); 
s_bar_min = min(diag(S_bar));
x_tls = (A'*A-s_bar_min^2*eye(1))^-1*A'*b;

x_ls = (A'*A)^-1*A'*b;

lw = 2;
colors = get_RGBColorCodes({'olive','grey','blue','red'});
A_continous = 0:0.1:2;
hold on
plot(A_continous,A_continous*x_ls, 'LineWidth',lw,'Color',colors(:,3),'DisplayName','\boldmath$x_{OLS}$');
% plot(A_continous,A_continous*x_tls, 'LineWidth',lw,'Color',colors(:,4),'DisplayName','$x_{TLS}$');

neg_slope = -(1/x_tls);
for i = 1:length(A)
    plot([A(i) A(i)],[b(i) A(i)*x_ls],'-.','HandleVisibility','off','LineWidth',1,'Color',colors(:,3))
%     y_intersec = -neg_slope*A(i)+b(i);
%     intersec = y_intersec/(x_tls-neg_slope)
%     plot([A(i) intersec],[b(i) intersec*x_tls],'-.','HandleVisibility','off','LineWidth',1,'Color',colors(:,4));  
end
scatter(A,b,[],colors(:,2)','o', 'filled','DisplayName','Samples','LineWidth',4);

legend('Location','NorthWest')
xlim([0,4/2.8]);ylim([0,1]);
xticks(0:0.2:4/2.8);yticks(0:0.2:1);
setSize([400 280]);
xlabel('\boldmath$a_{m}$')
ylabel('$b_{m}$')
setLineWidth(2);
setFontSize([11;11;11;11]);
grid on
xticklabels({''}),yticklabels({''})

grid on
box on
print('ls_sketch','-depsc')