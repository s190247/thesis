clear all; close all; clc
addpath(strcat('..',filesep, 'simulations', filesep, 'plotting', filesep, 'util'))
%# create x,y
xmax = 2;
ymax = 2;
mesh = 1;
[xx,yy]=ndgrid(0:mesh:xmax,0:mesh:ymax);
z_function = [0.1, 0.3];
% z_function = [0 0];
%# calculate corresponding z
z = z_function(1)*xx+z_function(2)*yy;
zmax = max(max(z));
margin = 0.1;
zoff = 0.5;
% Plot vectors
origin = [0, 0, 0];
headWidth = 8;
headLength = 8;
a1 = [1; 0; z(1+1/mesh,1)];
a2 = [0; 1; z(1,1+1/mesh)];
A = [a1, a2];
% Create vector b
b = [0.8*xmax; 0.8*ymax; zmax+zoff];
% b_proj1 = b'*a1/(a1'*a1)*a1;
% b_proj2 = b'*a2/(a2'*a2)*a2;
% b_proj = b_proj1+b_proj2;
x_ls = (A'*A)^-1*A'*b;
b_proj = A*x_ls;
res = b-b_proj;
%% # plot the surface
figure(1)
color = get_RGBColorCodes({'red'})';
col = zeros(size(xx));
surf(xx,yy,z, col, 'EdgeColor','none','FaceAlpha',0.2);
colormap(color);
view([45 45])
xlim([-margin, xmax+margin]);
ylim([-margin, ymax+margin]);
zlim([-margin, zmax+margin+zoff]);
hold on
% set(gca,'visible','off')
set(gca,'XColor', 'none','YColor','none', 'ZColor','none')

%%
headsize = 1;

x1_qui = quiver3(a1(1), a1(2), a1(3), a1(1)*(x_ls(1)-1), a1(2)*(x_ls(1)-1), a1(3)*(x_ls(1)-1),'k','LineWidth',2, 'AutoScale','off','MaxHeadSize',headsize/(norm(x_ls(1)-1)));
x2_qui = quiver3(a2(1), a2(2), a2(3), a2(1)*(x_ls(2)-1), a2(2)*(x_ls(2)-1), a2(3)*(x_ls(2)-1),'k','LineWidth',2, 'AutoScale','off','MaxHeadSize',headsize/(norm(x_ls(2)-1)));
plot3([a1(1)*x_ls(1), b_proj(1)],[a1(2)*x_ls(1), b_proj(2)],[a1(3)*x_ls(1), b_proj(3)],'k-.','LineWidth',1)
plot3([a2(1)*x_ls(2), b_proj(1)],[a2(2)*x_ls(2), b_proj(2)],[a2(3)*x_ls(2), b_proj(3)],'k-.','LineWidth',1)

a1_qui = quiver3(origin(1), origin(2), origin(3), a1(1), a1(2), a1(3),'r','LineWidth',2, 'AutoScale','off','MaxHeadSize',headsize/norm(a1));
a2_qui = quiver3(origin(1), origin(2), origin(3), a2(1), a2(2), a2(3), '-r','LineWidth',2, 'AutoScale','off','MaxHeadSize',headsize/norm(a2));
b_qui = quiver3(origin(1), origin(2), origin(3), b(1), b(2), b(3), 'k','AutoScale','off','LineWidth',2,'MaxHeadSize',headsize/norm(b));
b_proj_qui = quiver3(origin(1), origin(2), origin(3), b_proj(1), b_proj(2), b_proj(3), 'Color',0.6*ones(1,3),'AutoScale','off','LineWidth',2,'MaxHeadSize',headsize/norm(b_proj));
res_qui = quiver3(b_proj(1), b_proj(2), b_proj(3), res(1), res(2), res(3), 'AutoScale','off', 'Color',0.6*ones(1,3),'LineWidth',2,'MaxHeadSize',headsize/norm(res),'LineStyle','-');
fs = 14;
pos_a1 = 0.3*a1+[0;0;-0.3];
text(pos_a1(1),pos_a1(2), pos_a1(3),'{\boldmath$a$}$_1$','Interpreter','latex','FontSize',fs,'Color',a2_qui.Color)
pos_a2 = 0.3*a2+[0;0;0.4];
text(pos_a2(1), pos_a2(2), pos_a2(3),'{\boldmath$a$}$_2$','Interpreter','latex','FontSize',fs,'Color',a1_qui.Color)
pos_b = 0.85*b+[0;0;0.2];
text(pos_b(1), pos_b(2), pos_b(3),'\boldmath$b$','Interpreter','latex','FontSize',fs,'Color',b_qui.Color)
pos_b_proj = 0.8*b_proj+[0;0;0.15];
text(pos_b_proj(1), pos_b_proj(2), pos_b_proj(3),'\boldmath$\hat{b}$','Interpreter','latex','FontSize',fs,'Color',b_proj_qui.Color)
pos_res = b_proj+0.5*res+[0;0;0.2];
text(pos_res(1), pos_res(2), pos_res(3),'\boldmath$r$','Interpreter','latex','FontSize',fs,'Color',res_qui.Color)
pos_xls1 = pos_a1+[0.5,0,-0.1]';
text(pos_xls1(1), pos_xls1(2), pos_xls1(3),'{\boldmath$a$}$_1${\boldmath$x$}$_{LS}(1)$','Interpreter','latex','FontSize',fs,'Color',[0,0,0])
pos_xls2 = pos_a2+[0,0.3,+0.5]';
text(pos_xls2(1), pos_xls2(2), pos_xls2(3),'{\boldmath$a$}$_2${\boldmath$x$}$_{LS}(2)$','Interpreter','latex','FontSize',fs,'Color',[0,0,0])

%%
setSize([400 280]);
set(gca, 'Position', [0.02,0.02,0.95,0.95])
axis tight
print('projection_to_span','-depsc','-r1000')


% print('projection_to_span','-depsc','-r1000')

%% TLS
% [~, S_bar, ~] = svd([A,b]); 
% s_bar_min = min(diag(S_bar));
% x_tls = (A'*A-s_bar_min^2*eye(2))^-1*A'*b;
% tls_proj = A*x_tls;
% quiver3(0,0,0,tls_proj(1),tls_proj(2),tls_proj(3),'k','LineWidth',3, 'AutoScale','off');
