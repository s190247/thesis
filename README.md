# Sensitivity of the sound zones problem to sources of error
## Master's Thesis Colin Benker (s190247)
## Department of Acoustic Technology, Danmarks Tekniske Universitet (DTU)
This repository includes the code that was used to model the sound zone problem as desribed in the Master's Thesis *Sensitivity of the sound zones problem to sources of error*, which can also be found in this repository (*SensitivityOfSoundZonesProblemToSourcesOfError.pdf*. Each of the simulations found in the paper have their own script. 

The thesis provides a detailed explanation of the performed simulations. 

- **StatisticalPropertiesOfTFs.m** shows the statistical properties of the error model, as shown in figures 3.1 and 3.2
- **noRegularization.m** contains the script to create figure 4.1, i.e. the performance of the unregularized PM solution in the presence of different error sources
- **singularValuesConditionNumber.m** creates the singular values and condition number plots from figure 4.2
- **plotEffort.m** illustrates the control effort as shown in figure 4.3
- **ridgeRegression.m** performs the RR simulation and plots figures 4.4 and 4.5
- **tls.m** performs TLS and RTLS simulations as shown in figure 4.6
- **resmpledTemperature.m** contains the script to model the influence of temperature on the acoustic contrast and creates figure 4.7
- **pmo.m** performs the PMO simulations as illustrated in figure 4.8
- **pmoDiscussion.m** performs the PMO simulations from table 5.1
- **plotSPL.m** simulates the SPL in both sound zones to create figure 5.1

The sketches from section 2.4 (The Least Squares Problem) can be created by running the following scripts in the **sketches** folder

- **ls.m** Illustrates LS solution, as in figure 2.4a
- **ls_sensitivity.m** Illustrates LS sensitivity, as in figure 2.4b
- **tls_vs_ls.m** illustrates comparison between OLS and TLS as in figure 2.5 

The **plotting** folder contains functions to help plotting. The .eps files from the plotting are stored in **plotting/plots**. The **func** folder contains all functions to perform the simulations.