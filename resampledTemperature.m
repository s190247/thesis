clear all; close all;
addpath(genpath(pwd));

speakerErrors_condition = [false];
roomTempErrors_condition = [true];
mc_it = 200;
f = 1:300;
m_ac_res = zeros(length(speakerErrors_condition),length(f));
std_ac_res = zeros(length(speakerErrors_condition),length(f));
m_ac_opt = zeros(length(speakerErrors_condition),length(f));
std_ac_opt = zeros(length(speakerErrors_condition),length(f));
m_ac_0 = zeros(length(speakerErrors_condition),length(f));
std_ac_0 = zeros(length(speakerErrors_condition),length(f));
m_ac_resrr = zeros(length(speakerErrors_condition),length(f));
std_ac_resrr = zeros(length(speakerErrors_condition),length(f));
m_ac_res_error = zeros(length(speakerErrors_condition),length(f));
std_ac_res_error = zeros(length(speakerErrors_condition),length(f));

for it1 = 1:length(speakerErrors_condition)
    [Hsim, H0] = getTransFuncMat(speakerErrors_condition(it1), roomTempErrors_condition(it1), mc_it);
    [m_ac_res(it1,:), std_ac_res(it1,:)] = runSim(Hsim, H0, 'resample_temp', mc_it);
    [m_ac_opt(it1,:), std_ac_opt(it1,:)] = runSim(Hsim, H0, 'optimal', mc_it);
    [m_ac_0(it1,:), std_ac_0(it1,:)] = runSim(Hsim, H0, 'off', mc_it);
    [m_ac_resrr(it1,:), std_ac_resrr(it1,:)] = runSim(Hsim, H0, 'resample_temp_rr', mc_it, 1e-3);
    [m_ac_res_error(it1,:), std_ac_res_error(it1,:)] = runSim(Hsim, H0, 'resample_temp_error', mc_it, 1e-3);

    table_string = ac2latextable(m_ac_res(it1,:), std_ac_res(it1,:), speakerErrors_condition(it1), roomTempErrors_condition(it1), '0,res');
    disp(table_string)
    table_string = ac2latextable(m_ac_res_error(it1,:), std_ac_res_error(it1,:), speakerErrors_condition(it1), roomTempErrors_condition(it1), '0,res,err');
    disp(table_string)
    table_string = ac2latextable(m_ac_resrr(it1,:), std_ac_res_error(it1,:), speakerErrors_condition(it1), roomTempErrors_condition(it1), 'RR,res');
    disp(table_string)
end

%%
figure(17);
colors = get_RGBColorCodes({'pink','blue','teal'});
plot(m_ac_0,'DisplayName','{\boldmath${H}$}$_{R}${\boldmath${w}$}$_{0}$', 'LineWidth',2, 'Color', colors(:,2));hold on;
plot(m_ac_0-std_ac_0,'Color', colors(:,2), 'HandleVisibility','off', 'LineStyle',':');
plot(m_ac_0+std_ac_0,'Color', colors(:,2), 'HandleVisibility','off', 'LineStyle',':');

plot(m_ac_opt, 'DisplayName','{\boldmath${H}$}$_{R}${\boldmath${w}$}$_{opt}$', 'LineWidth',2, 'Color', 'k');
plot(m_ac_opt-std_ac_opt,'Color', 'k', 'HandleVisibility','off', 'LineStyle',':');
plot(m_ac_opt+std_ac_opt,'Color', 'k', 'HandleVisibility','off', 'LineStyle',':');

plot(m_ac_res, 'DisplayName','{\boldmath${H}$}$_{R}${\boldmath${w}$}$_{0,res}$', 'LineWidth',1, 'Color', colors(:,1)); 
plot(m_ac_res-std_ac_res,'Color', colors(:,1), 'HandleVisibility','off', 'LineStyle',':');
plot(m_ac_res+std_ac_res,'Color', colors(:,1), 'HandleVisibility','off', 'LineStyle',':');

% plot(m_ac_resrr,'DisplayName','{\boldmath${H}$}$_{R}${\boldmath${w}$}$_{RR,res}$', 'LineWidth',1, 'Color', colors(:,3));
plot(m_ac_res_error,'DisplayName','{\boldmath${H}$}$_{R}${\boldmath${w}$}$_{0,res,err}$', 'LineWidth',1, 'Color', colors(:,3));
plot(m_ac_res_error-std_ac_res_error,'Color', colors(:,3), 'HandleVisibility','off', 'LineStyle',':');
plot(m_ac_res_error+std_ac_res_error,'Color', colors(:,3), 'HandleVisibility','off', 'LineStyle',':');

grid on; xlim([20,300]); set(gca, 'XScale', 'log'); 
xticks([20,50,100,200,300]); 
legend('NumColumns',2, 'Location', 'SouthWest');
xlabel('Frequency [Hz]');
ylabel('Acoustic Contrast [dB]');
setSize([800 280]);
setFontSize([11;11;11;11]);
hold off
print(strcat('plotting',filesep,'plots',filesep,'resampledTemperatureAC', string(it1)),'-depsc');
