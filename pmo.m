addpath(genpath(pwd));
speakerErrors_condition = [false, true,  true, false];
roomTempErrors_condition = [false, true,  false, true];
colors = get_RGBColorCodes({'olive','red','blue'});
colors = [0,0,0; colors']';
colors2 = get_RGBColorCodes({'orange','lavender'});
colors3 = get_RGBColorCodes({'lavender'});
namesPMO = {'{\boldmath${H}$}$_{0}${\boldmath${w}$}$_{PMO}$', ...
         '{\boldmath${H}$}$_{SR}${\boldmath${w}$}$_{PMO,SR}$', ...
         '{\boldmath${H}$}$_{S}${\boldmath${w}$}$_{PMO,S}$', ...
         '{\boldmath${H}$}$_{R}${\boldmath${w}$}$_{PMO,R}$'};
names0 = {'{\boldmath${H}$}$_{0}${\boldmath${w}$}$_{0}$', ...
         '{\boldmath${H}$}$_{SR}${\boldmath${w}$}$_{0}$', ...
         '{\boldmath${H}$}$_{S}${\boldmath${w}$}$_{0}$', ...
         '{\boldmath${H}$}$_{R}${\boldmath${w}$}$_{0}$'};
namesopt = {'{\boldmath${H}$}$_{0}${\boldmath${w}$}$_{opt}$', ...
         '{\boldmath${H}$}$_{SR}${\boldmath${w}$}$_{opt}$', ...
         '{\boldmath${H}$}$_{S}${\boldmath${w}$}$_{opt}$', ...
         '{\boldmath${H}$}$_{R}${\boldmath${w}$}$_{opt}$'};
LineStyle = {'-','-','-','-'};
LineWidth = [2,2,2,2];

mc_it = 200;
m_ac_pmo = zeros(length(speakerErrors_condition), 300);
m_ac_opt = zeros(length(speakerErrors_condition), 300);
m_ac_0 = zeros(length(speakerErrors_condition), 300);
std_ac_pmo = zeros(length(speakerErrors_condition), 300);
std_ac_opt = zeros(length(speakerErrors_condition), 300);
std_ac_0 = zeros(length(speakerErrors_condition), 300);
m_repErr = zeros(length(speakerErrors_condition), 300);

for it1 = 2:4%length(speakerErrors_condition)
    [Hsim, H0] = getTransFuncMat(speakerErrors_condition(it1), roomTempErrors_condition(it1), mc_it);
    [m_ac_pmo(it1,:), std_ac_pmo(it1,:), m_repErr(it1,:)] = runSim(Hsim, H0, 'pmo', mc_it, [speakerErrors_condition(it1), roomTempErrors_condition(it1)]);
    [m_ac_opt(it1,:), std_ac_opt(it1,:)] = runSim(Hsim, H0, 'optimal', mc_it);
    [m_ac_0(it1,:), std_ac_0(it1,:)] = runSim(Hsim, H0, 'off', mc_it);
    %% Plot AC
    figure(it1+4);
    % Plot w0
    plot(m_ac_0(it1,:),'Color', colors(:,it1), 'DisplayName', char(names0(it1)), 'LineStyle',char(LineStyle(it1)),'LineWidth',LineWidth(it1));
    hold on;
    plot(m_ac_0(it1,:)-std_ac_0(it1,:),'Color', colors(:,it1), 'HandleVisibility','off', 'LineStyle',':');
    plot(m_ac_0(it1,:)+std_ac_0(it1,:),'Color', colors(:,it1), 'HandleVisibility','off', 'LineStyle',':'); 
    % Plot w_opt
    plot(m_ac_opt(it1,:),'Color', 'k', 'DisplayName', char(namesopt(it1)), 'LineStyle',char(LineStyle(it1)),'LineWidth',LineWidth(it1));
    plot(m_ac_opt(it1,:)-std_ac_opt(it1,:),'Color', 'k', 'HandleVisibility','off', 'LineStyle',':');
    plot(m_ac_opt(it1,:)+std_ac_opt(it1,:),'Color', 'k', 'HandleVisibility','off', 'LineStyle',':');
    % Plot best RR
    plot(m_ac_pmo(it1,:),'Color', colors3, 'DisplayName', char(namesPMO(it1)), 'LineStyle',char(LineStyle(it1)),'LineWidth',LineWidth(it1));
    plot(m_ac_pmo(it1,:)-std_ac_pmo(it1,:),'Color', colors3, 'HandleVisibility','off', 'LineStyle',':');
    plot(m_ac_pmo(it1,:)+std_ac_pmo(it1,:),'Color', colors3, 'HandleVisibility','off', 'LineStyle',':'); 
    
    grid on; xlim([20,300]); set(gca, 'XScale', 'log'); 
    xticks([20,50,100,200,300]); 
    legend('NumColumns',2, 'Location', 'SouthWest');
    xlabel('Frequency [Hz]');
    ylabel('Acoustic Contrast [dB]');
    setSize([800 280]);
    setFontSize([11;11;11;11]);
    hold off
    print(strcat('plotting',filesep,'plots',filesep,'ACPMO', string(it1)),'-depsc');
    
    %% Print to table
    table_string = ac2latextable(m_ac_pmo(it1,:), std_ac_pmo(it1,:), speakerErrors_condition(it1), roomTempErrors_condition(it1), 'PMO');
    disp(table_string)
end