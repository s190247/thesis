addpath(genpath(pwd));

speakerErrors_condition = [false, true,  true, false];
roomTempErrors_condition = [false, true,  false, true];
colors = get_RGBColorCodes({'olive','red','blue'});
colors = [0,0,0; colors']';
colors2 = get_RGBColorCodes({'orange','lavender'});
colors3 = get_RGBColorCodes({'lavender'});
namesPMO = {'{\boldmath${H}$}$_{0}${\boldmath${w}$}$_{PMO}$', ...
         '{\boldmath${H}$}$_{SR}${\boldmath${w}$}$_{PMO,SR}$', ...
         '{\boldmath${H}$}$_{S}${\boldmath${w}$}$_{PMO,S}$', ...
         '{\boldmath${H}$}$_{R}${\boldmath${w}$}$_{PMO,R}$'};
names0 = {'{\boldmath${H}$}$_{0}${\boldmath${w}$}$_{0}$', ...
         '{\boldmath${H}$}$_{SR}${\boldmath${w}$}$_{0}$', ...
         '{\boldmath${H}$}$_{S}${\boldmath${w}$}$_{0}$', ...
         '{\boldmath${H}$}$_{R}${\boldmath${w}$}$_{0}$'};
namesopt = {'{\boldmath${H}$}$_{0}${\boldmath${w}$}$_{opt}$', ...
         '{\boldmath${H}$}$_{SR}${\boldmath${w}$}$_{opt}$', ...
         '{\boldmath${H}$}$_{S}${\boldmath${w}$}$_{opt}$', ...
         '{\boldmath${H}$}$_{R}${\boldmath${w}$}$_{opt}$'};
LineStyle = {'-','-','-','-'};
LineWidth = [2,2,2,2];

mc_it = 200;
m_ac_pmo = zeros(length(speakerErrors_condition), 300);
m_ac_opt = zeros(length(speakerErrors_condition), 300);
m_ac_0 = zeros(length(speakerErrors_condition), 300);
std_ac_pmo = zeros(length(speakerErrors_condition), 300);
std_ac_opt = zeros(length(speakerErrors_condition), 300);
std_ac_0 = zeros(length(speakerErrors_condition), 300);
m_repErr = zeros(length(speakerErrors_condition), 300);
m_ac_pmoOver = zeros(length(speakerErrors_condition), 300);
std_ac_pmoOver = zeros(length(speakerErrors_condition), 300);
m_ac_pmoUnder = zeros(length(speakerErrors_condition), 300);
std_ac_pmoUnder = zeros(length(speakerErrors_condition), 300);
m_ac_zhu01 = zeros(length(speakerErrors_condition), 300);
std_ac_zhu01 = zeros(length(speakerErrors_condition), 300);
m_ac_zhu1 = zeros(length(speakerErrors_condition), 300);
std_ac_zhu1 = zeros(length(speakerErrors_condition), 300);
m_ac_zhu05 = zeros(length(speakerErrors_condition), 300);
std_ac_zhu05 = zeros(length(speakerErrors_condition), 300);
m_ac_zhu3 = zeros(length(speakerErrors_condition), 300);
std_ac_zhu3 = zeros(length(speakerErrors_condition), 300);

for it1 = 2:2%length(speakerErrors_condition)
    [Hsim, H0] = getTransFuncMat(speakerErrors_condition(it1), roomTempErrors_condition(it1), mc_it);
    [m_ac_pmo(it1,:), std_ac_pmo(it1,:)] = runSim(Hsim, H0, 'pmo', mc_it, [speakerErrors_condition(it1), roomTempErrors_condition(it1)]);
    [m_ac_pmoOver(it1,:), std_ac_pmoOver(it1,:)] = runSim(Hsim, H0, 'pmoA', mc_it, 151);
    [m_ac_pmoUnder(it1,:), std_ac_pmoUnder(it1,:)] = runSim(Hsim, H0, 'pmoA', mc_it, 152);
    [m_ac_opt(it1,:), std_ac_opt(it1,:), repErr] = runSim(Hsim, H0, 'optimal', mc_it);
    [m_ac_0(it1,:), std_ac_0(it1,:)] = runSim(Hsim, H0, 'off', mc_it);
    [m_ac_zhu01(it1,:), std_ac_zhu01(it1,:)] = runSim(Hsim, H0, 'pmoZhu', mc_it, {'a',0.1});
    [m_ac_zhu1(it1,:), std_ac_zhu1(it1,:)] = runSim(Hsim, H0, 'pmoZhu', mc_it, {'a',1});
    [m_ac_zhu05(it1,:), std_ac_zhu05(it1,:)] = runSim(Hsim, H0, 'pmoZhu', mc_it, {'a',0.5});
    [m_ac_zhu3(it1,:), std_ac_zhu3(it1,:)] = runSim(Hsim, H0, 'pmoZhu', mc_it, {'a',3});

    %% Plot AC
%     figure(it1+4);
    figure;
    % Plot w0
    plot(m_ac_0(it1,:),'Color', colors(:,it1), 'DisplayName', char(names0(it1)), 'LineStyle',char(LineStyle(it1)),'LineWidth',LineWidth(it1));
    hold on;
    plot(m_ac_0(it1,:)-std_ac_0(it1,:),'Color', colors(:,it1), 'HandleVisibility','off', 'LineStyle',':');
    plot(m_ac_0(it1,:)+std_ac_0(it1,:),'Color', colors(:,it1), 'HandleVisibility','off', 'LineStyle',':'); 
    % Plot w_opt
    plot(m_ac_opt(it1,:),'Color', 'k', 'DisplayName', char(namesopt(it1)), 'LineStyle',char(LineStyle(it1)),'LineWidth',LineWidth(it1));
    plot(m_ac_opt(it1,:)-std_ac_opt(it1,:),'Color', 'k', 'HandleVisibility','off', 'LineStyle',':');
    plot(m_ac_opt(it1,:)+std_ac_opt(it1,:),'Color', 'k', 'HandleVisibility','off', 'LineStyle',':');
    % Plot PMO
    plot(m_ac_pmo(it1,:),'Color', colors3, 'DisplayName', char(namesPMO(it1)), 'LineStyle',char(LineStyle(it1)),'LineWidth',LineWidth(it1));
    plot(m_ac_pmo(it1,:)-std_ac_pmo(it1,:),'Color', colors3, 'HandleVisibility','off', 'LineStyle',':');
    plot(m_ac_pmo(it1,:)+std_ac_pmo(it1,:),'Color', colors3, 'HandleVisibility','off', 'LineStyle',':'); 
    % Plot PMOOver
%     plot(m_ac_pmoOver(it1,:),'Color', 'r', 'DisplayName', 'over', 'LineStyle',char(LineStyle(it1)),'LineWidth',LineWidth(it1));
%     plot(m_ac_pmoOver(it1,:)-std_ac_pmoOver(it1,:),'Color', 'r', 'HandleVisibility','off', 'LineStyle',':');
%     plot(m_ac_pmoOver(it1,:)+std_ac_pmoOver(it1,:),'Color', 'r', 'HandleVisibility','off', 'LineStyle',':');
    % Plot PMOUnder
%     plot(m_ac_pmoUnder(it1,:),'Color', 'c', 'DisplayName', 'Under', 'LineStyle',char(LineStyle(it1)),'LineWidth',LineWidth(it1));
%     plot(m_ac_pmoUnder(it1,:)-std_ac_pmoUnder(it1,:),'Color', 'c', 'HandleVisibility','off', 'LineStyle',':');
%     plot(m_ac_pmoUnder(it1,:)+std_ac_pmoUnder(it1,:),'Color', 'c', 'HandleVisibility','off', 'LineStyle',':');
    % Plot PMOZhu01
    plot(m_ac_zhu01(it1,:),'Color', 'b', 'DisplayName', '0.1', 'LineStyle',char(LineStyle(it1)),'LineWidth',LineWidth(it1));
    plot(m_ac_zhu01(it1,:)-std_ac_zhu01(it1,:),'Color', 'b', 'HandleVisibility','off', 'LineStyle',':');
    plot(m_ac_zhu01(it1,:)+std_ac_zhu01(it1,:),'Color', 'b', 'HandleVisibility','off', 'LineStyle',':');
    % Plot PMOZhu05
    plot(m_ac_zhu05(it1,:),'Color', 'r', 'DisplayName', '0.5', 'LineStyle',char(LineStyle(it1)),'LineWidth',LineWidth(it1));
    plot(m_ac_zhu05(it1,:)-std_ac_zhu05(it1,:),'Color', 'r', 'HandleVisibility','off', 'LineStyle',':');
    plot(m_ac_zhu05(it1,:)+std_ac_zhu05(it1,:),'Color', 'r', 'HandleVisibility','off', 'LineStyle',':');
     % Plot PMOZhu1
    plot(m_ac_zhu1(it1,:),'Color', 'm', 'DisplayName', '1', 'LineStyle',char(LineStyle(it1)),'LineWidth',LineWidth(it1));
    plot(m_ac_zhu1(it1,:)-std_ac_zhu1(it1,:),'Color', 'm', 'HandleVisibility','off', 'LineStyle',':');
    plot(m_ac_zhu1(it1,:)+std_ac_zhu1(it1,:),'Color', 'm', 'HandleVisibility','off', 'LineStyle',':');
     % Plot PMOZhu01
    plot(m_ac_zhu3(it1,:),'Color', 'c', 'DisplayName', '3', 'LineStyle',char(LineStyle(it1)),'LineWidth',LineWidth(it1));
    plot(m_ac_zhu3(it1,:)-std_ac_zhu3(it1,:),'Color', 'c', 'HandleVisibility','off', 'LineStyle',':');
    plot(m_ac_zhu3(it1,:)+std_ac_zhu3(it1,:),'Color', 'c', 'HandleVisibility','off', 'LineStyle',':');
      
    grid on; xlim([20,300]); set(gca, 'XScale', 'log'); 
    xticks([20,50,100,200,300]); 
    legend('NumColumns',2, 'Location', 'SouthWest');
    xlabel('Frequency [Hz]');
    ylabel('Acoustic Contrast [dB]');
    setSize([800 280]);
    setFontSize([11;11;11;11]);
    hold off
    print(strcat('plotting',filesep,'plots',filesep,'ACPMODiscussion', string(it1)),'-depsc');
    
    %% Print to table
    table_string = ac2latextable(m_ac_pmo(it1,:), std_ac_pmo(it1,:), speakerErrors_condition(it1), roomTempErrors_condition(it1), 'PMO');
    disp(table_string)
    table_string = ac2latextable(m_ac_pmoOver(it1,:), std_ac_pmoOver(it1,:), speakerErrors_condition(it1), roomTempErrors_condition(it1), 'PMO_{Over}');
    disp(table_string)
    table_string = ac2latextable(m_ac_pmoUnder(it1,:), std_ac_pmoUnder(it1,:), speakerErrors_condition(it1), roomTempErrors_condition(it1), 'PMO_{Under}');
    disp(table_string)
    table_string = ac2latextable(m_ac_zhu01(it1,:), std_ac_zhu01(it1,:), speakerErrors_condition(it1), roomTempErrors_condition(it1), 'PMO_{ME0.1}');
    disp(table_string)
    table_string = ac2latextable(m_ac_zhu05(it1,:), std_ac_zhu05(it1,:), speakerErrors_condition(it1), roomTempErrors_condition(it1), 'PMO_{ME0.5}');
    disp(table_string)
    table_string = ac2latextable(m_ac_zhu1(it1,:), std_ac_zhu1(it1,:), speakerErrors_condition(it1), roomTempErrors_condition(it1), 'PMO_{ME1}');
    disp(table_string)
    table_string = ac2latextable(m_ac_zhu3(it1,:), std_ac_zhu3(it1,:), speakerErrors_condition(it1), roomTempErrors_condition(it1), 'PMO_{ME3}');
    disp(table_string)
end