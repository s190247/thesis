addpath(genpath(pwd));

[~, H0] = getTransFuncMat(false,false,0);
[s, s_bar] = getSingularValues(H0);
%%
plotSingularValues(s,s_bar);
%%
condition_numberH2 = (s(1,:)./s(end,:)).^2;
plotConditionNumber(condition_numberH2);
