addpath(genpath(pwd));

speakerErrors_condition = [false, true,  true, false];
roomTempErrors_condition = [false, true,  false, true];
colors = get_RGBColorCodes({'olive','red','blue'});
colors = [0,0,0; colors']';
colors2 = get_RGBColorCodes({'orange','lavender'});
colors3 = get_RGBColorCodes({'grey'});
namesRR = {'{\boldmath${H}$}$_{0}${\boldmath${w}$}$_{RR}$', ...
         '{\boldmath${H}$}$_{SR}${\boldmath${w}$}$_{RR}$', ...
         '{\boldmath${H}$}$_{S}${\boldmath${w}$}$_{RR}$', ...
         '{\boldmath${H}$}$_{R}${\boldmath${w}$}$_{RR}$'};
names0 = {'{\boldmath${H}$}$_{0}${\boldmath${w}$}$_{0}$', ...
         '{\boldmath${H}$}$_{SR}${\boldmath${w}$}$_{0}$', ...
         '{\boldmath${H}$}$_{S}${\boldmath${w}$}$_{0}$', ...
         '{\boldmath${H}$}$_{R}${\boldmath${w}$}$_{0}$'};
namesopt = {'{\boldmath${H}$}$_{0}${\boldmath${w}$}$_{opt}$', ...
         '{\boldmath${H}$}$_{SR}${\boldmath${w}$}$_{opt}$', ...
         '{\boldmath${H}$}$_{S}${\boldmath${w}$}$_{opt}$', ...
         '{\boldmath${H}$}$_{R}${\boldmath${w}$}$_{opt}$'};
LineStyle = {'-','-','-','-'};
LineWidth = [2,2,2,2];

mc_it = 200;
lambdas = logspace(-5, 2, 100);
m_ac_ti = zeros(length(speakerErrors_condition), length(lambdas), 300);
m_ac_opt = zeros(length(speakerErrors_condition), 300);
m_ac_0 = zeros(length(speakerErrors_condition), 300);
std_ac_ti = zeros(length(speakerErrors_condition), length(lambdas), 300);
std_ac_opt = zeros(length(speakerErrors_condition), 300);
std_ac_0 = zeros(length(speakerErrors_condition), 300);
m_repErr = zeros(length(speakerErrors_condition), length(lambdas), 300);

best_lambda = zeros(4, 300);
for it1 = 2:4%length(speakerErrors_condition)
    [Hsim, H0] = getTransFuncMat(speakerErrors_condition(it1), roomTempErrors_condition(it1), mc_it);
    for it2 = 1:length(lambdas) 
        [m_ac_ti(it1,it2,:), std_ac_ti(it1,it2,:), m_repErr(it1,it2,:)] = runSim(Hsim, H0, 'tikhonov', mc_it, lambdas(it2));
    end
    [m_ac_opt(it1,:), std_ac_opt(it1,:)] = runSim(Hsim, H0, 'optimal', mc_it);
    [m_ac_0(it1,:), std_ac_0(it1,:)] = runSim(Hsim, H0, 'off', mc_it);

    %% Plot best lambda
    figure(it1);
    imagesc(20:size(H0,3), log10(lambdas), squeeze(m_repErr(it1,:,20:end)));
    colormap bone
    [~,best_lambda(it1,:)] = min(squeeze(m_repErr(it1,:,:)));
    hold on;
    % Plot 1dB from max
    clear i
    [i(:,2),i(:,1)] = find(squeeze(m_repErr(it1,:,:)) < min(squeeze(m_repErr(it1,:,:)))+1);
    max_lambda1dB = zeros(size(H0,3),1);
    min_lambda1dB = zeros(size(H0,3),1);
    for f = 1:size(H0,3)
        max_lambda1dB(f) = max(i(i(:,1)==f,2));
        min_lambda1dB(f) = min(i(i(:,1)==f,2));
    end
    plot(20:size(H0,3), log10(lambdas(max_lambda1dB(20:end))),'Color','r')
    plot(20:size(H0,3), log10(lambdas(min_lambda1dB(20:end))),'Color','r')
    plot(20:size(H0,3), log10(lambdas(best_lambda(it1,20:end))),'Color','g')
    ylim([-5.01 2])
    yticks(-5:1:3)
    xticks([20,50,100,200,300])
    xlabel('Frequency [Hz]')
    ylabel('$log_{10}(\lambda)$')
    c=colorbar('TickLabelInterpreter','latex');
    c.Label.String = 'Reproduction Error [dB]';
    c.Label.Interpreter = 'latex';
    setFontSize([11,11,11,11]);
    legend off;
    setSize([800, 280])
    print(strcat('plotting',filesep,'plots',filesep,'optimalRRParamRepErr',string(it1)),'-depsc');

    %% Plot AC
    best_idx = sub2ind(size(squeeze(m_ac_ti(it1,:,:))), best_lambda(it1,:), 1:length(best_lambda));
    m_best_ac = m_ac_ti(it1,best_idx);
    std_best_ac = std_ac_ti(it1,best_idx);
    figure(it1+4);
    % Plot w0
    plot(m_ac_0(it1,:),'Color', colors(:,it1), 'DisplayName', char(names0(it1)), 'LineStyle',char(LineStyle(it1)),'LineWidth',LineWidth(it1));
    hold on;
    plot(m_ac_0(it1,:)-std_ac_0(it1,:),'Color', colors(:,it1), 'HandleVisibility','off', 'LineStyle',':');
    plot(m_ac_0(it1,:)+std_ac_0(it1,:),'Color', colors(:,it1), 'HandleVisibility','off', 'LineStyle',':'); 
    % Plot w_opt
    plot(m_ac_opt(it1,:),'Color', 'k', 'DisplayName', char(namesopt(it1)), 'LineStyle',char(LineStyle(it1)),'LineWidth',LineWidth(it1));
    plot(m_ac_opt(it1,:)-std_ac_opt(it1,:),'Color', 'k', 'HandleVisibility','off', 'LineStyle',':');
    plot(m_ac_opt(it1,:)+std_ac_opt(it1,:),'Color', 'k', 'HandleVisibility','off', 'LineStyle',':');
    % Plot best RR
    plot(m_best_ac,'Color', colors3, 'DisplayName', char(namesRR(it1)), 'LineStyle',char(LineStyle(it1)),'LineWidth',LineWidth(it1));
    plot(m_best_ac-std_best_ac,'Color', colors3, 'HandleVisibility','off', 'LineStyle',':');
    plot(m_best_ac+std_best_ac,'Color', colors3, 'HandleVisibility','off', 'LineStyle',':'); 
    
    grid on; xlim([20,300]); set(gca, 'XScale', 'log'); 
    xticks([20,50,100,200,300]); 
    legend('NumColumns',2, 'Location', 'SouthWest');
    xlabel('Frequency [Hz]');
    ylabel('Acoustic Contrast [dB]');
    setSize([800 280]);
    setFontSize([11;11;11;11]);
    hold off
    print(strcat('plotting',filesep,'plots',filesep,'ACTikhonovCondRepErr', string(it1)),'-depsc');
    
    %% Print to table
    table_string = ac2latextable(m_best_ac, std_best_ac, speakerErrors_condition(it1), roomTempErrors_condition(it1), 'RR');
    disp(table_string)
end
