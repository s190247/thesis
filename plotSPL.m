addpath(genpath(pwd));

speakerErrors_condition = [false, true,  true, false];
roomTempErrors_condition = [false, true,  false, true];
colors = get_RGBColorCodes({'olive','red','blue', 'grey'});
colors = [0,0,0; colors']';
colors2 = get_RGBColorCodes({'orange','lavender'});
colors3 = 1.5*get_RGBColorCodes({'lavender'});
namesPMO = {'{\boldmath${H}$}$_{0}${\boldmath${w}$}$_{PMO}$', ...
         '{\boldmath${H}$}$_{SR}${\boldmath${w}$}$_{PMO,SR}$', ...
         '{\boldmath${H}$}$_{S}${\boldmath${w}$}$_{PMO,S}$', ...
         '{\boldmath${H}$}$_{R}${\boldmath${w}$}$_{PMO,R}$'};
names0 = {'{\boldmath${H}$}$_{0}${\boldmath${w}$}$_{0}$', ...
         '{\boldmath${H}$}$_{SR}${\boldmath${w}$}$_{0}$', ...
         '{\boldmath${H}$}$_{S}${\boldmath${w}$}$_{0}$', ...
         '{\boldmath${H}$}$_{R}${\boldmath${w}$}$_{0}$'};
namesopt = {'{\boldmath${H}$}$_{0}${\boldmath${w}$}$_{opt}$', ...
         '{\boldmath${H}$}$_{SR}${\boldmath${w}$}$_{opt}$', ...
         '{\boldmath${H}$}$_{S}${\boldmath${w}$}$_{opt}$', ...
         '{\boldmath${H}$}$_{R}${\boldmath${w}$}$_{opt}$'};
namesRR = {'{\boldmath${H}$}$_{0}${\boldmath${w}$}$_{RR}$', ...
         '{\boldmath${H}$}$_{SR}${\boldmath${w}$}$_{RR}$', ...
         '{\boldmath${H}$}$_{S}${\boldmath${w}$}$_{RR}$', ...
         '{\boldmath${H}$}$_{R}${\boldmath${w}$}$_{RR}$'};
LineStyle = {'-','-','-','-'};
LineWidth = [2,2,2,2];

mc_it = 200;
f = 1:300;
M = 150;
m_SPL_pmo = zeros(length(speakerErrors_condition), M, length(f));
m_SPL_opt = zeros(length(speakerErrors_condition), M, length(f));
m_SPL_0 = zeros(length(speakerErrors_condition), M, length(f));
std_SPL_pmo = zeros(length(speakerErrors_condition), M, length(f));
std_SPL_opt = zeros(length(speakerErrors_condition), M, length(f));
std_SPL_0 = zeros(length(speakerErrors_condition), M, length(f));
m_repErr = zeros(length(speakerErrors_condition), M, length(f));
m_SPL_RR = zeros(length(speakerErrors_condition), M, length(f));
std_SPL_RR = zeros(length(speakerErrors_condition), M, length(f));
m_SPL_res = zeros(length(speakerErrors_condition), M, length(f));
std_SPL_res = zeros(length(speakerErrors_condition), M, length(f));

for it1 = 2%length(speakerErrors_condition)
    [Hsim, H0] = getTransFuncMat(speakerErrors_condition(it1), roomTempErrors_condition(it1), mc_it);
    [~,~,~,~, m_SPL_pmo(it1,:,:), std_SPL_pmo(it1,:,:)] = runSim(Hsim, H0, 'pmo', mc_it, [speakerErrors_condition(it1), roomTempErrors_condition(it1)]);
    [~,~,~,~, m_SPL_opt(it1,:,:), std_SPL_opt(it1,:,:)] = runSim(Hsim, H0, 'optimal', mc_it);
    [~,~,~,~, m_SPL_0(it1,:,:), std_SPL_0(it1,:,:)] = runSim(Hsim, H0, 'off', mc_it);
    [~,~,~,~, m_SPL_RR(it1,:,:), std_SPL_RR(it1,:,:)] = runSim(Hsim, H0, 'tikhonov', mc_it, 10^(-2.8));

    %% Plot SPL
    mB_SPL_opt = squeeze(mean(m_SPL_opt(it1,1:M/2,:),2));
    stdB_SPL_opt = squeeze(mean(std_SPL_opt(it1,1:M/2,:),2));
    mB_SPL_0 = squeeze(mean(m_SPL_0(it1,1:M/2,:),2));
    stdB_SPL_0 = squeeze(mean(std_SPL_0(it1,1:M/2,:),2));
    mB_SPL_pmo = squeeze(mean(m_SPL_pmo(it1,1:M/2,:),2));
    stdB_SPL_pmo = squeeze(mean(std_SPL_pmo(it1,1:M/2,:),2));
    mB_SPL_RR = squeeze(mean(m_SPL_RR(it1,1:M/2,:),2));
    stdB_SPL_RR = squeeze(mean(std_SPL_RR(it1,1:M/2,:),2));
    
    
    mD_SPL_opt = squeeze(mean(m_SPL_opt(it1,M/2+1:end,:),2));
    stdD_SPL_opt = squeeze(mean(std_SPL_opt(it1,M/2+1:end,:),2));
    mD_SPL_0 = squeeze(mean(m_SPL_0(it1,M/2+1:end,:),2));
    stdD_SPL_0 = squeeze(mean(std_SPL_0(it1,M/2+1:end,:),2));
    mD_SPL_pmo = squeeze(mean(m_SPL_pmo(it1,M/2+1:end,:),2));
    stdD_SPL_pmo = squeeze(mean(std_SPL_pmo(it1,M/2+1:end,:),2));
    mD_SPL_RR = squeeze(mean(m_SPL_RR(it1,M/2+1:end,:),2));
    stdD_SPL_RR = squeeze(mean(std_SPL_RR(it1,M/2+1:end,:),2));
    figure(it1+4);
    % Plot w0
    plot(mB_SPL_0,'Color', colors(:,it1), 'DisplayName', char(names0(it1)), 'LineStyle',char(LineStyle(it1)),'LineWidth',LineWidth(it1));
    hold on;
    plot(mB_SPL_0-stdB_SPL_0,'Color', colors(:,it1), 'HandleVisibility','off', 'LineStyle',':');
    plot(mB_SPL_0+stdB_SPL_0,'Color', colors(:,it1), 'HandleVisibility','off', 'LineStyle',':'); 
    % Plot w_opt
    plot(mB_SPL_opt,'Color', 'k', 'DisplayName', char(namesopt(it1)), 'LineStyle',char(LineStyle(it1)),'LineWidth',LineWidth(it1));
    plot(mB_SPL_opt-stdB_SPL_opt,'Color', 'k', 'HandleVisibility','off', 'LineStyle',':');
    plot(mB_SPL_opt+stdB_SPL_opt,'Color', 'k', 'HandleVisibility','off', 'LineStyle',':');
    % Plot best PMO
    plot(mB_SPL_pmo,'Color', colors3, 'DisplayName', char(namesPMO(it1)), 'LineStyle',char(LineStyle(it1)),'LineWidth',2);
    plot(mB_SPL_pmo-stdB_SPL_pmo,'Color', colors3, 'HandleVisibility','off', 'LineStyle',':');
    plot(mB_SPL_pmo+stdB_SPL_pmo,'Color', colors3, 'HandleVisibility','off', 'LineStyle',':'); 
    % Plot best RR
    plot(mB_SPL_RR,'Color', 0.6*colors(:,5), 'DisplayName', char(namesRR(it1)), 'LineStyle',char(LineStyle(it1)),'LineWidth',1);
    plot(mB_SPL_RR-stdB_SPL_RR,'Color', 0.6*colors(:,5), 'HandleVisibility','off', 'LineStyle',':');
    plot(mB_SPL_RR+stdB_SPL_RR,'Color', 0.6*colors(:,5), 'HandleVisibility','off', 'LineStyle',':'); 
    
    plot(mD_SPL_0,'Color', colors(:,it1), 'HandleVisibility','off', 'LineStyle',char(LineStyle(it1)),'LineWidth',LineWidth(it1));
    hold on;
    plot(mD_SPL_0-stdD_SPL_0,'Color', colors(:,it1), 'HandleVisibility','off', 'LineStyle',':');
    plot(mD_SPL_0+stdD_SPL_0,'Color', colors(:,it1), 'HandleVisibility','off', 'LineStyle',':'); 
    % Plot w_opt
    plot(mD_SPL_opt,'Color', 'k', 'HandleVisibility','off', 'LineStyle',char(LineStyle(it1)),'LineWidth',LineWidth(it1));
    plot(mD_SPL_opt-stdD_SPL_opt,'Color', 'k', 'HandleVisibility','off', 'LineStyle',':');
    plot(mD_SPL_opt+stdD_SPL_opt,'Color', 'k', 'HandleVisibility','off', 'LineStyle',':');
    % Plot best PMO
    plot(mD_SPL_pmo,'Color', colors3, 'HandleVisibility','off', 'LineStyle',char(LineStyle(it1)),'LineWidth',2);
    plot(mD_SPL_pmo-stdD_SPL_pmo,'Color', colors3, 'HandleVisibility','off', 'LineStyle',':');
    plot(mD_SPL_pmo+stdD_SPL_pmo,'Color', colors3, 'HandleVisibility','off', 'LineStyle',':'); 
    % Plot RR
    plot(mD_SPL_RR,'Color', 0.6*colors(:,5), 'HandleVisibility','off', 'LineStyle',char(LineStyle(it1)),'LineWidth',1);
    plot(mD_SPL_RR-stdD_SPL_RR,'Color', 0.6*colors(:,5), 'HandleVisibility','off', 'LineStyle',':');
    plot(mD_SPL_RR+stdD_SPL_RR,'Color',0.6* colors(:,5), 'HandleVisibility','off', 'LineStyle',':'); 
    
    grid on; xlim([20,300]); set(gca, 'XScale', 'log'); 
    xticks([20,50,100,200,300]); 
    legend('NumColumns',2, 'Location', 'SouthEast');
    xlabel('Frequency [Hz]');
    ylabel('SPL [dB]');
    setSize([800 330]);
    setFontSize([11;11;11;11]);
    hold off
    print(strcat('plotting',filesep,'plots',filesep,'SPLtot', string(it1)),'-depsc');
end