function plotAngleImage(img, freq)
%PLOTANGLEIMAGE Summary of this function goes here
%   Detailed explanation goes here
addpath(fullfile(pwd, 'plotting', 'util'))
figure(1)
for i=1:length(freq)
    subplot(1, length(freq), i)
    imagesc(squeeze(img(i,:,:))); caxis([0 90]); 
    xlabel('$l$')
    ax = gca;
    if i == 1
        ylabel('$l$')
        
%         width = ax.Position[
    else
        ylabel('')
    end
    xticks(1:8);
    yticks(1:8);
    title(strcat(string(freq(i)),'~Hz'))
    setFontSize([11;11;11;11]);    
    legend('off')
end
colormap('hot')
setSize([800 280]);

c = colorbar('Position',[ax.Position(1)+ax.Position(3)+0.01, ax.Position(2), 0.025, ax.Position(4)]);
c.Label.String = 'Angle [$^\circ$]';
c.Label.FontSize = 13;
c.Label.Interpreter = 'latex';

c.TickLabelInterpreter = 'latex';
% ax.FontSize = 11;

print('AngleImage','-depsc')

end

