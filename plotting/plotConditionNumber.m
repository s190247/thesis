function plotConditionNumber(cn)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
figure;
addpath(fullfile(pwd, 'plotting', 'util'))

plot(cn,'Color','k','LineStyle','-','LineWidth',1.5);
% title('Tikhonov Regularization in varying Temperature')
title('')
xticks([20, 50, 100, 200, 300])
grid on;
xlim([20,300])
% ylim(10.^([-2.7,1.5]))
xlabel('Frequency [Hz]')
ylabel('Condition Number')
set(gca, 'XScale', 'log')
set(gca, 'YScale', 'log')
% setLineWidth(1);
setSize([400 280]);
setFontSize([11;11;11;11]);
legend('off')
print(strcat('plotting',filesep,'plots',filesep,'conditionNumber'),'-depsc');
hold off
end

