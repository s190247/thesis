function plotqlp1m(p)
%PLOTH2026 plots pressure at 1m from loudspeaker
figure(264);
colors = get_RGBColorCodes({'blue','red'});
hold on;
plot(p);
% set(gca, 'XScale', 'log'); xlim([20, 300]); grid on; xticks([20, 50, 100, 200, 300]);
% plot(h20, 'Color', colors(:,1), 'DisplayName', 'T = 20 $^{\circ}$C');

legend('Location','SouthWest', 'NumColumns',1)
title('')
xlabel('Frequency [Hz]')
ylabel('$q_l$ [dB]')
setLineWidth(2);
setSize([400 280]);
setFontSize([11;11;11;11]);
xlim([20,300]); xticks([20,50,100,200,300]); ylim([65, 82]);
hold off;
grid on;
% print(strcat('plotting',filesep,'plots',filesep,'h2026'),'-depsc');
end

