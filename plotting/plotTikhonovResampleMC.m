function plotTikhonovResampleMC(ac_ti_r, ac_r, ac_opt, ac)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
addpath(fullfile(pwd, 'plotting', 'util'))

figure
colors = get_RGBColorCodes({'grey','blue','red', 'olive'});
colors = 1.1*colors;
% colors(:,4) = 0.*colors(:,4);
plot(ac_opt,'DisplayName','{\boldmath${w}$}$_{opt}$','Color',colors(:,1),'LineStyle','-');
hold on
plot(ac_r,'DisplayName','{\boldmath${w}$}$_{0,res}$','Color',colors(:,4),'LineStyle','-');
plot(ac_ti_r,'DisplayName','{\boldmath${w}$}$_{RR,res}$','Color',1.2*colors(:,2),'LineStyle','--');
plot(ac,'DisplayName','{\boldmath${w}$}$_{0}$','Color',colors(:,3),'LineStyle','-');

grid on
legend('Location','South', 'NumColumns',2)
% title('Tikhonov Regularization in varying Temperature')
title('')
xticks([20, 50, 100, 200, 300])
yticks(-20:10:50)
xlim([20,300])
ylim([0,49])
xlabel('Frequency [Hz]')
ylabel('Acoustic Contrast [dB]')
set(gca, 'XScale', 'log')
setLineWidth(2);
setSize([400 280]);
setFontSize([11;11;11;11]);

print('ResTikhMC','-depsc')
hold off

end

