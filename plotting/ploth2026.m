function ploth2026(h20, h26, h23)
%PLOTH2026 plots transfer function at 20 and 26 ?C
figure(49);
colors = get_RGBColorCodes({'blue', 'red'});
plot(h26, 'Color', colors(:,2), 'DisplayName', '$h_{r26^{\circ}C}$');
hold on;
set(gca, 'XScale', 'log'); xlim([20, 300]); grid on; xticks([20, 50, 100, 200, 300]);
plot(h20, 'Color', colors(:,1), 'DisplayName', '$h_{r20^{\circ}C}$');

legend('Location','SouthWest', 'NumColumns',1)
title('')
xlabel('Frequency [Hz]')
ylabel('$h_{ml}$ [dB]')
setLineWidth(1);
setSize([400 280]);
setFontSize([11;11;11;11]);
print(strcat('plotting',filesep,'plots',filesep,'h2026'),'-depsc');
hold off;

figure(50)
plot(h26-h23, 'Color', colors(:,1), 'DisplayName', '$h_{r26^{\circ}C}-h_{r23^{\circ}C}$')
hold on
plot(h20-h23, 'Color', colors(:,2), 'DisplayName', '$h_{r20^{\circ}C}-h_{r23^{\circ}C}$')
legend('Location','NorthWest', 'NumColumns',1)
title('')
xlabel('Frequency [Hz]')
ylabel('Level Difference [dB]')
setLineWidth(1);
setSize([400 280]);
setFontSize([11;11;11;11]);
grid on
print(strcat('plotting',filesep,'plots',filesep,'h2026Diff'),'-depsc');
hold off
end

