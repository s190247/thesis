function [] = plotTikhonovMC(ac1, m_ac_ti, std_ac_ti, m_ac, std_ac)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
addpath(fullfile(pwd, 'plotting', 'util'))

figure(1)
colors = get_RGBColorCodes({'grey','blue','red'});
colors = 1.3*colors;
plot(ac1,'DisplayName','{\boldmath${w}$}$_{opt}$','Color',colors(:,1),'LineStyle','-');
hold on
plot(m_ac_ti,'DisplayName','{\boldmath${w}$}$_{RR}$','Color',colors(:,2),'LineStyle','-');
plot(m_ac,'DisplayName','{\boldmath${w}$}$_{0}$','Color',colors(:,3),'LineStyle','-');

grid on
legend('Location','SouthWest', 'NumColumns',1,'FontSize',2)
% title('Tikhonov Regularization in varying Temperature')
title('')
xticks([20, 50, 100, 200, 300])
yticks(-20:10:50)
xlim([20,300])
ylim([0,49])
xlabel('Frequency [Hz]')
ylabel('Acoustic Contrast [dB]')
set(gca, 'XScale', 'log')
setLineWidth(2);
setSize([400 280]);
setFontSize([11;11;11;11]);

print('TikhonovAC','-depsc')
hold off
end

