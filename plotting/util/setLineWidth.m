function setLineWidth(lw, ax)
%SETLINEWIDTH Summary of this function goes here
%   Detailed explanation goes here
if nargin<2
    ax = gca;
end
h = get(ax, 'Children');
set(h, 'LineWidth', lw);
end

