function setFontSize(FS,ax, fig)
%SETFONTSIZE FS [TitFS, X/Y-Label, X/Y-Ticks, Legend]
if nargin < 3
    fig = gcf;
end
if nargin < 2
    ax = gca;
end
if nargin < 1
    FS = [22; 16; 19; 17];
end
if size(FS) == [1,1]
    FS = ones(4,1)*FS;
end
layout = 1.25; % Number of columns in latex document - Use 1.25 for "Conference Paper" 2-column template
FS = FS*layout;
ax.FontSize = FS(3); set(gca, 'TickLabelInterpreter', 'Latex');
htit = get(ax, 'title'); set(htit, 'FontSize', FS(1),'Interpreter','Latex'); 
title_str = strcat('\textbf{',htit.String,'}'); title(title_str);
hx = get(ax, 'xlabel'); set(hx, 'FontSize', FS(2),'Interpreter','Latex');
hy = get(ax, 'ylabel'); set(hy, 'FontSize', FS(2),'Interpreter','Latex');
hl = get(ax, 'legend'); set(hl, 'FontSize', FS(4)); legend('interpreter','latex')
end

