function [] = plotTikhonovMC(acti, ac1, lambdas)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
addpath(fullfile(pwd, 'util'))

figure
colors = get_RGBColorCodes({'blue','red'});
colors = 1.3*colors;
plot(lambdas, acti,'DisplayName','{\boldmath${w}$}$_{RR}$','Color',colors(:,1),'LineStyle','-');
hold on
% plot(lambdas, ac1,'DisplayName','{\boldmath${w}$}$_{RR,opt}$','Color',0.5*colors(:,1),'LineStyle',':');

legend('Location','SouthWest', 'NumColumns',1)
% title('Tikhonov Regularization Parameter Tuning')
title('')

xlabel('$\lambda$')
ylabel('Mean AC [dB]')
ylim([5,29])
set(gca, 'XScale', 'log')
setLineWidth(2);
% lambda_max = lambdas(21);
% xti = 10.^(-10:5);
% xtila = {'$10^{-10}$','','','','','$10^{-5}$','','','','','$10^0$','','','','','10^5'};
% set(gca, 'XTick', xti, 'XTickLabel', xtila)
% set(gca, 'XTick', sort([lambda_max, get(gca, 'XTick')]));
% set(gca, 'XtickLabels',{'$10^{-10}$', '$10^{-5}$', '$\lambda_{max}$', '$10^{0}$', '$10^[5}$'})
ax = gca;
% set(ax.XAxis.TickLabelChild,'String',ax.XAxis.TickLabels)

% xlim([-inf, 1.5e5])

setSize([400 280]);
setFontSize([11;11;11;11]);
grid on

print('LambdaAC','-depsc')
hold off
end

