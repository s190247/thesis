function plotSoundField2D(pos_grid, p_mat, freq_vec, locs, savefile,clim, plotSoundZones)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
if nargin < 7
    plotSoundZones = true;
end
fig = figure;
FS = 12;
set(fig, 'Position', [100, 100, 680, 192])
speakersize = .2;
locs(1,locs(1,:)==0) = locs(1,locs(1,:)==0)+speakersize/2;
locs(2,locs(2,:)==0) = locs(2,locs(2,:)==0)+speakersize/2;
locs(1,locs(1,:)>=max(pos_grid(1,:))) = locs(1,locs(1,:)>=max(pos_grid(1,:)))-speakersize/2;
locs(2,locs(2,:)>=max(pos_grid(2,:))) = locs(2,locs(2,:)>=max(pos_grid(2,:)))-speakersize/2;

for it = 1:length(freq_vec)
    subplot(1,3,it)
    freq = freq_vec(it);
%     max_freq = max(max((:,:,freq)
    imagesc('XData',pos_grid(1,:), 'YData', pos_grid(2,:), 'CData',p_mat(:,:,freq)');
    view([90 -90])% set(gca,'YDir','normal')
    ax = gca;
    axis('equal')
    xlim([0,max(pos_grid(1,:))]); xticks(0:1:max(pos_grid(1,:)));
    ylim([0,max(pos_grid(2,:))]); yticks(0:1:max(pos_grid(2,:)));  
    caxis(clim)
    colormap('bone'); 
    if it > 1
        xticks([]);
        ax.Position(1) = ax_prev.Position(1)+ax_prev.Position(3)+0.01;
        ax.Position(2:3) = ax_prev.Position(2:3);
    else
        ax.Position(1) = 0.02;
        ax.Position(3) = 1/(length(freq_vec)+.45);
%         ax.Position(4) = ax.Position(3)*max(pos_grid(1,:))/max(pos_grid(2,:));
    end
    ax_prev = gca;
    
    if plotSoundZones 
        % Add rectangles for sound zones
        rectangle('Position', [1.914-.2, 4.24-.2, .4, .4],...
          'EdgeColor','k', 'LineWidth', 1);
        text(1.914-.08, 4.24+.35, 'B', 'FontSize', 11, 'Interpreter', 'latex');

        rectangle('Position', [4.14-.2, 4.35-.2, .4, .4],...
          'EdgeColor',[0.7 0.7 0.7], 'LineWidth', 1);
        text(4.14-.08, 4.35+.35, 'D', 'FontSize', 11, 'Interpreter', 'latex', 'Color', [0.7 0.7 0.7]);
    end
    
    % Add rectangles for speakers
    for it2 = 1:size(locs,2)
        rectangle('Position', [locs(1,it2)-speakersize/2, locs(2,it2)-speakersize/2, speakersize speakersize], 'FaceColor',[.7 .7 .7]);
    end
    title(strcat(string(freq), '~Hz'), 'Interpreter', 'Latex', 'FontSize', FS);
    set(ax, 'FontSize', FS, 'TickLabelInterpreter', 'Latex');
end
cb = colorbar('Position',[ax.Position(1)+ax.Position(3)+0.01, ax.Position(2), 0.025, ax.Position(4)], ...
               'FontSize', FS, 'TickLabelInterpreter', 'Latex');

print(savefile,'-depsc')
hold off
end

