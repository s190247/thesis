function plotTLS(acopt, ac0, acTLS)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
figure(1)
colors = get_RGBColorCodes({'grey','blue','red','olive'});
colors = 1.3*colors;
plot(acopt,'DisplayName','{\boldmath${w}$}$_{opt}$','Color',colors(:,1),'LineStyle','-');
hold on
plot(ac0,'DisplayName','{\boldmath${w}$}$_{0}$','Color',colors(:,3),'LineStyle','-');
plot(acTLS,'DisplayName','{\boldmath${w}$}$_{TLS}$','Color',0.8*colors(:,4),'LineStyle',':');
% plot(acTLS0,'DisplayName','{\boldmath${H}$}$_{0}${\boldmath${w}$}$_{TLS}$','Color',colors(:,4)*0.8,'LineStyle',':');

grid on
legend( 'Location','SouthWest','NumColumns',1)
% title('Tikhonov Regularization in varying Temperature')
title('')
xticks([20, 50, 100, 200, 300])
yticks(-20:10:50)
xlim([20,300])
ylim([0,49])
xlabel('Frequency [Hz]')
ylabel('Acoustic Contrast [dB]')
set(gca, 'XScale', 'log')
setLineWidth(2);
setSize([400 280]);
setFontSize([11;11;11;11]);

print('TLSAC','-depsc')
hold off
end