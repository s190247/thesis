function plotSingularValues(s, s_bar)
%PLOTSINGULARVALUES Summary of this function goes here
%   Detailed explanation goes here
addpath(fullfile(pwd, 'plotting', 'util'))

figure()
colors = get_RGBColorCodes({'grey','blue','red'});
colors = 1.3*colors;
plot(s(1,:),'Color',colors(:,1),'LineStyle','-','DisplayName','$\sigma_l$','LineWidth',1.5);
hold on
plot(s(2:end,:)','Color',colors(:,1),'LineStyle','-','HandleVisibility','off','LineWidth',1.5);
plot(s_bar(end,:),'k','DisplayName','$\bar{\sigma}_{L+1}$','LineWidth',1.5)
grid on
% title('Tikhonov Regularization in varying Temperature')
title('')
xticks([20, 50, 100, 200, 300])
yticks(10.^(-2:1:1))
xlim([20,300])
ylim(10.^([-2.7,1.5]))
xlabel('Frequency [Hz]')
ylabel('Singular Values')
set(gca, 'XScale', 'log')
set(gca, 'YScale', 'log')
% setLineWidth(1);
setSize([400 280]);
setFontSize([11;11;11;11]);
legend('Location','SouthEast')

print(strcat('plotting',filesep,'plots',filesep,'singularValues'),'-depsc');
hold off
end

