function plotTikhonovResampleMCTempErr(ac_ti0, ac_01, ac_05, ac_1, ac)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
addpath(fullfile(pwd, 'plotting', 'util'))

figure(52)
colors = get_RGBColorCodes({'blue'});
colors = 1.3*colors;
h1=plot(ac_ti0-ac_ti0,'DisplayName','{\boldmath${w}$}$_{RR,res}$','Color',colors,'LineStyle','--');
hold on
% colors = 0.8*colors;
% plot(ac_01-ac_ti0,'DisplayName','$E$[{\boldmath${H}$}$_{MC}${\boldmath${w}$}$_{\sigma_T=.1}$]','Color',colors,'LineStyle','-');
% colors = 0.8*colors;
% plot(ac_05-ac_ti0,'DisplayName','$E$[{\boldmath${H}$}$_{MC}${\boldmath${w}$}$_{\sigma_T=.5}$]','Color',colors,'LineStyle','-');
colors = 0.4*colors;
plot(ac_1-ac_ti0,'DisplayName','{\boldmath${w}$}$_{\sigma_T=1}$','Color',colors,'LineStyle','-');
olive = get_RGBColorCodes({'red'})*1.1;
plot(ac-ac_ti0,'DisplayName','{\boldmath${w}$}$_{0}$','Color',olive,'LineStyle','-');

grid on
legend('Location','South', 'NumColumns',2)
% title('Tikhonov Regularization in varying Temperature')
title('')
xticks([20, 50, 100, 200, 300])
yticks(-20:10:50)
xlim([20,300])
ylim([-30,10])
xlabel('Frequency [Hz]')
ylabel('Acoustic Contrast [dB]')
set(gca, 'XScale', 'log')
setLineWidth(2);
setSize([400 280]);
setFontSize([11;11;11;11]);
uistack(h1,'top');

print('ResTikhMCTempErr','-depsc')
hold off

end

