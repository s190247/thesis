function plotBestLambdaNormTLS(best_lambda_norm)
figure()
colors = get_RGBColorCodes({'grey'});
colors = 1.3*colors;
plot(best_lambda_norm,'Color',colors(:,1),'DisplayName','$\lambda_{TLS,opt}$','LineStyle','-');
grid on
legend( 'Location','SouthWest','NumColumns',1)
% title('Tikhonov Regularization in varying Temperature')
title('')
xticks([20, 50, 100, 200, 300])
yticks(-20:10:50)
xlim([20,300])
ylim([0,1.2])
xlabel('Frequency [Hz]')
ylabel('$\lambda$')
yticks(0:1/6:1)
yticklabels({'0','','','$\frac{\bar{\sigma}_{L+1}^2}{2}$','','','$\bar{\sigma}_{L+1}^2$'})
set(gca, 'XScale', 'log')
setLineWidth(2);
setSize([400 280]);
setFontSize([11;11;11;11]);

print('lambdanormTLS','-depsc')
hold off
end
