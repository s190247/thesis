function [s, s_bar] = getSingularValues(H, pt)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
s = zeros(size(H,2),size(H,3));
if nargout > 1
    s_bar = zeros(size(H,2)+1, size(H,3));
end
if nargin < 2 % If not specified otherwise assume bright zone is microphones 1:length/2 are bright zone and rest dark zone
    if nargout > 1
    pt = createTargetSoundField(squeeze(H));
    end
end

for f = 1:size(H,3)
    Hf = squeeze(H(:,:,f));
    [~, S, ~] = svd(Hf);
    s(:,f) = diag(S);
    if nargout > 1
    [~, S_bar, ~] = svd([Hf, pt(:,f)]);
    s_bar(:,f) = diag(S_bar);
    end
end
end

