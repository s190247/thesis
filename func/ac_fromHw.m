function [ac] = ac_fromHw(H,w)
%ac_fromHw calculates acoustic contrast when transfer function matrix H and
%filters w are given as input
p = zeros(size(H,1),size(H,3));
for fit = 1:size(H,3)
    p(:,fit) = H(:,:,fit)*w(:,fit);
end
ac = 10*log10(acoustic_contrast(p(1:size(H,1)/2,:),p(size(H,1)/2+1:end,:)));
end

