function pt = createTargetSoundField(H, standing_wave, locm)
%Creates standing wave pattern in bright zones and 0 sound pressure in dark
%zone for each frequency f assuming that the first half of the microphones
%are in the bright zone and the other half are in the dark zone
if nargin < 2
    standing_wave = false;
end
if nargin < 3
    [~, ~, locm] = setup_locations();
end
global c
if standing_wave
    direction = 1;  % Direction of standing wave
    distance = sqrt(sum((locm(direction,1:size(locm,2)/2)-locm(direction,1)).^2,1));
    phase_shift = distance'*f*2*pi/c;

    pt_b = cos(phase_shift)+1j*sin(phase_shift);
    pt_d = zeros(size(locm,2)/2, length(f));
    pt = [pt_b; pt_d];
else % Create target sound field from speaker number 7
    pt = zeros(size(locm,2),size(H,3)); 
    [dim, locs, locm] = setup_locations()
    H = transFuncMat(locs, locm, dim, 1:300, 0.6);

    pt(1:size(locm,2)/2,:) = squeeze(H(1:size(locm,2)/2,7,:));
%     pt = zeros(size(locm,2),length(f));
%     pt(1:size(pt,1)/2,:) = 1;
    
end
end

