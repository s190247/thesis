function [H2D] = create2DSpatialImage(H1D)
%CREATE2DSPATIALIMAGE Takes input transfer function matrix at a given
%frequency and z-position to create a 2D Image of H which makes spatial
%correlation visible.
% This script assumes that the number of microphones in the y-direction is
% 5
n = 5; % Number of microphones in y direction
H2D = reshape(H1D,n,length(H1D)/n);
H2D = [H2D(:,1:n);H2D(:,n+1:end)];
end

