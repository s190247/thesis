function [T] = random_temp(Tmin, Tmax, mc_iterations, random_seed)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
if nargin < 4
    random_seed = 0;
end
rng(random_seed);
if nargin < 5
    dist = 'uniform';
end
if strcmp(dist, 'uniform')
    r = round(rand(mc_iterations,1),2);    % Uniformly distributed temperature
else
    r = round(randn(mc_iterations,1),2);
end

T = Tmin+(Tmax-Tmin) * r;   % Random temperature between Tmin and Tmax
% T = 295+2*r
end

