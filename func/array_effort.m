function eff = array_effort(H,w)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
pt = createTargetSoundField(H);
w_ref = pmFilterRef(H(:,7,:),pt);
eff = zeros(size(w,2),1);
for f = 1:size(w,2)
    eff(f) = w(:,f)'*w(:,f)/(w_ref(f)'*w_ref(f));
end
eff = 10*log10(abs(eff))
end

