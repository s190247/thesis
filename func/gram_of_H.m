function H2 = gram_of_H(H)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
H2 = zeros(size(H,2),size(H,2), size(H,3));
for f = 1:size(H,3)
    Hf = squeeze(H(:,:,f));
    H2(:,:,f) = Hf'*Hf;
end
end

