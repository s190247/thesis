function [cn] = conditionNumberH(H)
%conditionNumberH returns condition number cn of H'H (NOT H)
cn = zeros(1,size(H,3));
for f = 1:size(H,3)
    cn(f) = cond(squeeze(H(:,:,f))'*squeeze(H(:,:,f)));
end
    
end

