function [w] = pmFilterStoch(H, H0)
%PMFILTERMARTIN calculates filter w that minimizes expected reproduction
%error for a given set of transfer function matrices H(N x M x L x f),
%where N is the number of transfer function matrices
pt = createTargetSoundField(H0);

w = zeros(size(H,3),size(H,4));
GramHf = zeros(size(H,1), size(H,3), size(H,3));

for f = 1:size(pt,2)
    Hf = squeeze(H(:,:,:,f));
    for n = 1:size(Hf,1)
        Hfn = squeeze(Hf(n,:,:));
        GramHf(n,:,:) = Hfn'*Hfn;
    end
    expected_Hf = squeeze(mean(Hf,1));
    expected_GramHf = squeeze(mean(GramHf,1));
    
    w(:,f) = (expected_GramHf)^(-1)*expected_Hf'*pt(:,f);
end
end


