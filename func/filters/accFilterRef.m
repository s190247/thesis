function [w] = accFilterRef(H)
%Reference implementation of the ACC filter according to equation 24 in
%Martins paper and assuming the upper half of H is H_Bright and lower half
%is H_Dark
HB = H(1:size(H,1)/2,:,:);
HD = H(size(H,1)/2+1:end,:,:);
w = zeros(size(H,2),size(H,3));
for f = 1:size(w,2)
    HDf = HD(:,:,f);
    HBf = HB(:,:,f);
    [w(:,f),~] = eigs( (HDf'*HDf)^(-1)*(HBf'*HBf)   ,1);
end

end

