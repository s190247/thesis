wfunction w = pmFilterRegTLS(H, lambda_norm, pt)
% Calculate PM/LS solution with regularized TLS given a
% regularization parameter lambda_norm that is the regularization parameter relative to sigma_{N+1}^2,
% the transfer function matrix H and
% the target sound field pt
if nargin < 3 % If not specified otherwise assume bright zone is microphones 1:length/2 are bright zone and rest dark zone
    pt = createTargetSoundField(H);
end

w = zeros(length(lambda_norm),size(H,2),size(H,3));
for f = 1:size(pt,2)
    Hf = H(:,:,f);
    [~, S_bar, ~] = svd([Hf, pt(:,f)]);
    s_bar_min = min(diag(S_bar));
    lambda = lambda_norm*s_bar_min^2;
    for l = 1:length(lambda)        
        w(l,:,f) = (Hf'*Hf+(lambda(l)-s_bar_min^2)*eye(size(H,2),size(H,2)))^(-1)*Hf'*pt(:,f);
    end
end
end