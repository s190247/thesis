function [w,s,s_bar] = pmFilterTLS(H, lambda, pt)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
w = zeros(size(H,2),size(H,3));
if nargin < 3
    pt = createTargetSoundField(H);
end
if nargin < 2
    lambda = 0;
end

s = zeros(size(H,2),size(pt,2));
s_bar = zeros(size(H,2)+1,size(pt,2));
for f = 1:size(pt,2)
    Hf = H(:,:,f);
    [~, S_bar, ~] = svd([Hf, pt(:,f)]);
    s_bar(:,f) = diag(S_bar);
    s_bar_min = min(s_bar(:,f));
    [~, S, ~] = svd(Hf);
    s(:,f) = diag(S);
    w(:,f) = (Hf'*Hf+((lambda-1)*s_bar_min^2)*eye(size(H,2),size(H,2)))^(-1)*Hf'*pt(:,f);
end
end

