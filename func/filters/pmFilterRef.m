function [w] = pmFilterRef(H, pt)
%Reference implementation of the pm filter according to equation 23 in
%Martins paper
w = zeros(size(H,2),size(H,3));
if nargin < 2 % If not specified otherwise assume bright zone is microphones 1:length/2 are bright zone and rest dark zone
%     pt = zeros(size(H,1),size(H,3));
%     pt(1:size(pt,1)/2,:) = 1;
    pt = createTargetSoundField(H);
end
% for s = 1:size(pt,1)
for f = 1:size(pt,2)
    Hf = H(:,:,f);
    w(:,f) = (Hf'*Hf)^(-1)*Hf'*pt(:,f);
end
end

