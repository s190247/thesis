function [w] = pmFilterTikhonov(H, lambda, pt)
% Calculate PM/LS solution with tikhonov regularization given a
% regularization parameter lambda, the transfer function matrix H and
% the target sound field pt
if nargin < 3 % If not specified otherwise assume bright zone is microphones 1:length/2 are bright zone and rest dark zone
    pt = createTargetSoundField(H);
end

w = zeros(size(H,2),size(H,3));
for f = 1:size(pt,2)
    Hf = H(:,:,f);
    w(:,f) = (Hf'*Hf+lambda*eye(size(H,2)))^(-1)*Hf'*pt(:,f);
end
end