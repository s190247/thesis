function w = pmFilterPMONoise(H0, param);
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
if param{1} == 'a'
    dBNoise = param{2};
end

Hpmo = zeros([160, size(H0)]);

for it = 1:size(Hpmo,1)
    for it2 = 1:size(Hpmo,2)
        Hpmo(it,it2,:,:) = squeeze(H0(it2,:,:)) + squeeze(H0(it2,:,:)).*(10.^(rand(size(H0,2), size(H0,3))*dBNoise/10)+exp(1i*(rand(size(H0,2), size(H0,3))*dBNoise/10)));
%         Hpmo(it,it2,:,:) = squeeze(H0(it2,:,:)) + squeeze(H0(it2,:,:)).*(10.^(rand(size(H0,2), size(H0,3))*dBNoise/10));
    end
end
%         disp('delete me in pmFilterPMONOIse')

w = pmFilterStoch(Hpmo,H0);
end

