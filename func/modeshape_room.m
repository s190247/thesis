function [phi] = modeshape_room(p, mode, dim)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
x = p(1); y = p(2); z = p(3);
nx = mode(1); ny = mode(2); nz = mode(3);
lx = dim(1); ly = dim(2); lz = dim(3);

if nx == 0
    epsilonnx = 1;
else
    epsilonnx = 2;
end
if ny == 0
    epsilonny = 1;
else
    epsilonny = 2;
end
if nz == 0
    epsilonnz = 1;
else
    epsilonnz = 2;
end

phi = sqrt(epsilonny*epsilonnx*epsilonnz)*cos(nx*pi*x/lx)*cos(ny*pi*y/ly)*cos(nz*pi*z/lz); 
end

