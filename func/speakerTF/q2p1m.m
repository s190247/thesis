function p = q2p1m(q, f)
%Volume velocity q to sound pressure in 1 m in free field conditions
global rho;
global c;
if nargin <2
    f = 1:length(q);
end
k = 2*pi*f/c;
r = 1;
p = 1j*2*pi*f*rho.*q.*exp(-1j*k*r)/(2*pi*r); % Low frequency standing on the floor!
end

