function [Hnew] = phaseError(H, mode, std_error)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
if nargin < 3
    mode = 'random';
end
Hnew = zeros(size(H));

    for l = 1:size(H,2)
        if mode == 'delay'  
            delay = randn(1)*std_error;
            phaseError = exp(1j*delay*(1:size(H,3)));
        elseif mode =='random'
            phaseError = exp(1j*randn(1:size(H,3)));
        end
        Hnew(:,l,:) = phaseError.*squeeze(H(:,l,:));
    end    
end

