function [q, p] = transfuncq(f,Re, CMS)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
% from https://www.tymphany.com/transducers/sls-p830668/
global rho; global c
if isempty(rho)
    rho = 1.204; %Density air
end
if isempty(c)
    c = 343;
end
if nargin <1
    f = 1:300;
end
if nargin <2
    Re = 5.61; % [Ohm]
end
if nargin <3
    CMS = 429e-6; % Suspension compliance from diaphragm
end
s = 1j*2*pi*f;
LE = 1.37e-3; % Voice coil inductance [H]
% Re = Re+LE*omega./ (1+LE/Re*omega);
SD = 346.4e-4; % Piston Area of diaphragm [m^2]
Bl = 10.42;% Magnetic flux density B * effective length of coil l = force factor Bl [Tm]
RAE = Bl^2./(SD^2*Re);  % 7.5
% Acoustic mass of diaphragm and air load on both sides of diaphragm
% Mas from equation 6.9
MMD = 56.3e-3; % Total moving mass of coil [kg]
MAD = MMD/SD^2; % Acoustic impedance of mass of coil
MA1 = 8*rho/(3*pi^2*sqrt(SD/pi));
Bbox = 0.65; %Example Leach (below eq 7.4)
rho_box = 7.18; % Example 2, sec. 7.1. lower limit: 1.18kg/m^3, upper lim: 7.18kg/m^3
MAB = Bbox*rho_box/(pi*sqrt(SD/pi));  %eq 7.1
MAC = MAD+MA1+MAB;  %7.5

% Total acoustic compliance
VB = 20e-3; % Volume box [m^3]
VAB = 1.15*VB;  % Acoustic volume with filling (In practice never greater than 20% (Leach)
CAB = VAB/(rho*c^2);
CAS = CMS*SD^2;
% CAT = CAB+CAS;% Total compliance of box and diaphragm suspension
CAT = CAS*CAB/(CAS+CAB);
%
RAB = 1780; % Acoustic impedance of box (taken from table 7.3)
QMS = 7.15;  % Mechanical Q factor
RAS = 1/QMS*sqrt(MAD/CAS); % Acoustic impedance of suspension of diaphragm, 6.15
RATC = RAE+RAB+RAS;  % Sum of acoustic resistances
omegac = 1/(sqrt(MAC*CAT));  % Resonance frequency
QTC = (1./RATC).*sqrt(MAC/CAT);   % Total closed box Q-factor (7.10)
% Equation 4.1 in Leach:
eg = 1; %[V]
q = (SD*eg/Bl) * (RAE./RATC) .* ((s./(QTC*omegac)) ./ ((s/omegac).^2 + s./(QTC*omegac) +1));
if nargout > 1
    GC = (s/omegac).^2./((s/omegac).^2 + (1/QTC * s/omegac) + 1); % 7.19
    omega_up = Re*MAC/(LE*MAD);     % Upper cutoff
    Tup = 1./(1+s/omega_up);    % Lowpass
    p = rho*Bl*eg/(2*pi*SD*Re*MAC) * GC .*Tup;
end
end

