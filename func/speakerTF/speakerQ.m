clear all; clc
addpath(genpath(fullfile(pwd, 'funcSpeaker')))
addpath(genpath(fullfile(pwd, '..', 'func')))
addpath(genpath(fullfile(pwd, '..', 'plotting')))
%% Initialise parameters
global c rho
rho = 1.204; %Density air
f = 1:1:300;
[dim, locs, locm] = setup_locations();
t60 = 0.6;
T0 = 296; 
c = speedofsound(T0);

%% Initial filter
if isfile(fullfile(pwd, 'data', 'H0.mat'))
    disp('Loading H0');
    load(fullfile(pwd, 'data', 'H0.mat'));
    H0 = H0(:,:,f);
else
    H0 = transFuncMat(locs, locm, dim, f, t60);
end

%% Initialise speaker
f_c0 = 30;
Q0 = 0.3;
G0 = speakerTF(f, f_c0, Q0); 
w0 = pmFilterRef(H0.*reshape(G0,1,1,length(G0)));

%% Set of Tikhonov Filters
lambdas = logspace(-9, 5, 50);
wti = zeros(length(lambdas), size(H0,2),size(H0,3));
ac1ti = zeros(length(lambdas), size(H0,3));
for it = 1:length(lambdas)
    wti(it,:,:) = pmFilterTikhonov(H0,lambdas(it));
    ac1ti(it,:) = ac_fromHw(H0, squeeze(wti(it,:,:)));
end
%% Monte Carlo
mc_it = 50;
ac0 = zeros(mc_it, size(H0,3));
ac_ti = zeros(mc_it, length(lambdas), size(H0,3));
ac_ti_opt = zeros(mc_it, length(lambdas), size(H0,3));
ac_opt = zeros(mc_it, size(H0,3));
Hnew = zeros(size(H0));
rng(0);
std_Q = 0.01; 
std_fc = 0.1*f_c0;
for it = 1:mc_it
    for l = 1:size(H0,2)
%         Qnew = Q0+std_Q*randn(1);
        Qnew = Q0;
        f_cnew = f_c0 + std_fc*randn(1);
        Gnew = speakerTF(f, f_cnew, Qnew); 
        Hnew(:,l,:) = H0(:,l,:).*reshape(Gnew,1,1,length(Gnew));
    end
    w_opt = pmFilterRef(Hnew);
    for it2 = 1:length(lambdas)        
        w_ti_opt = pmFilterTikhonov(Hnew,lambdas(it2));
        ac_ti_opt(it, it2, :) = ac_fromHw(Hnew, w_ti_opt);
        ac_ti(it, it2, :) = ac_fromHw(Hnew, squeeze(wti(it2, :, :)));
    end    
    ac0(it,:) = ac_fromHw(Hnew, w0);
    ac_opt(it,:) = ac_fromHw(Hnew, w_opt);
    printprogress(it,mc_it);
end

%% Select best lambda and calculate mean and std
[~, lambda_best] = max(mean(mean(ac_ti,3),1));
% lambda_best = 18
m_ac_ti = squeeze(mean(ac_ti(:,lambda_best,:), 1))';
std_ac_ti = squeeze(std(ac_ti(:,lambda_best,:), 1))';
m_ac = mean(ac0,1);
m_ac_opt = mean(ac_opt,1);
std_ac = std(ac0,1);
%%
plotTikhonovMC(m_ac_opt, m_ac_ti, std_ac_ti, m_ac, std_ac);
h1 = gca;
plotLambdaAC(mean(mean(ac_ti,3),1),  mean(mean(ac_ti_opt,3),1), lambdas);
h2 = gca;

%% Plot Singular Values
% s = getSingularValues(H0);
% plotSingularValues(s');

%% Print string table
str_opt = ac2latextable(m_ac_opt, 'opt')
str_RR0 = ac2latextable(m_ac_ti-m_ac_opt, 'Ridge regression filter $\bm{w}_{RR}$')
str_w0 = ac2latextable(m_ac-m_ac_opt, 'w0')