function q = speakerTF(T, f)
%spakerTF calculates speaker Transfer function for a given temperature
%vector of a voice coil at the frequencies f
% T: temperature vector of voice coil
q = zeros(length(T), length(f));
RE0 = 5.61;
alpha_copper = 0.004041; % Taken from https://www.allaboutcircuits.com/textbook/direct-current/chpt-12/temperature-coefficient-resistance/
RE = RE0*(1+alpha_copper*(T-23));

CMS0 = 429e-6; % Suspension compliance from diaphragm
CMS = CMS0*(1+0.25*(T-23)/40);   % Assume compliance changes by 25% when temperature is 40 degrees above reference temp 20?

for it = 1:length(T)
    q(it,:) = transfuncq(f, RE(it), CMS(it));
end
q0 = 0.0011; % I came up with this reference value to scale the volume velocities to have an average of 0 dB
q = q/q0;
end

