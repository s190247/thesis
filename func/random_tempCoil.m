function [T] = random_tempCoil(mc_iterations, random_seed, num_speakers)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
if nargin < 2
    random_seed = 0;
end
if nargin < 3
    num_speakers = 8;
end
rng(random_seed);

T = 23+3*gamrnd(2,2,mc_iterations,num_speakers);
% T = 30+2*gamrnd(2,2,mc_iterations,num_speakers);
end

