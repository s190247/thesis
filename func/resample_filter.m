function wnew = resample_filter(w,T0,Tnew)
%RESAMPLE_FILTER resamples filter that was calculated at T0 to match
%temperature at Tnew
precision = 1e-9;
[nom, denom] = rat(sqrt(Tnew)/sqrt(T0), precision);
while nom*denom > 2^31
    precision = 10*precision;
    [nom, denom] = rat(sqrt(Tnew)/sqrt(T0), precision);
end

wnew = resample(w',nom,denom)';
if Tnew > T0
    wnew = wnew(:,1:length(w));
end
end

