function printprogress(it,mc_it)
%PRINTPROGRESS Summary of this function goes here
%   Detailed explanation goes here
first_text = 'MC Iteration: ';
if it == 1
    delete_string = '\n';
else
    delete_length = length(strcat(first_text, sprintf('%d/%d',it-1,mc_it)));
    delete_string = repmat('\b', 1,delete_length+1);
end
new_string = strcat(delete_string, first_text, sprintf(' %d/%d',it,mc_it));
fprintf(new_string);

end

