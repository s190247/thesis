function [repErr, repErrB, repErrD] = repErr_fromHw(H, w, pt)
%ac_fromHw calculates acoustic contrast when transfer function matrix H and
%filters w are given as input
if nargin < 3 % If not specified otherwise assume bright zone is microphones 1:length/2 are bright zone and rest dark zone
    pt = createTargetSoundField(H);
end

p = zeros(size(H,1),size(H,3));

for fit = 1:size(H,3)
    p(:,fit) = H(:,:,fit)*w(:,fit);
end
repErr = 10*log10(sqrt(sum(abs(pt-p).^2,1)));
if nargout >2
    repErrB = 10*log10(sqrt(sum(abs(pt(1:size(pt,1)/2,:)-p(1:size(p,1)/2,:)).^2)));
    repErrD = 10*log10(sqrt(sum(abs(pt(size(pt,1)/2+1:end,:)-p(size(p,1)/2+1:end,:)).^2)));
end
end