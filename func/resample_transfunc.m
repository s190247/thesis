function hnew = resample_transfunc(h,T0,Tnew)
%RESAMPLE_FILTER resamples filter that was calculated at T0 to match
%temperature at Tnew
precision = 1e-9;
[nom, denom] = rat(sqrt(Tnew)/sqrt(T0), precision);
while nom*denom > 2^31
    precision = 10*precision;
    [nom, denom] = rat(sqrt(Tnew)/sqrt(T0), precision);
end

hnew = resample(h,nom,denom);
if Tnew > T0
    hnew = hnew(1:length(h));
end
end