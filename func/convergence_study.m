function [cs, average] = convergence_study(a)
%Shows how big the error is between the average of a(1:n) minus the average
%of a(1:n+1). 
cs = zeros(length(a)-1,1);
average = zeros(length(a),1);
for it = 1:length(a)
    average(it) = mean(a(1:it));
    if it>1
        cs(it) = average(it)-average(it-1);
    end
end
end

