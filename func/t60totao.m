function [tao] = t60totao(t60)
%Calculates time constant from a given reverberation time according to Lab2
%in Advanced Acoustics
% It would be more precise to define the reverberation time constant
% according to equation 8.75 in Fundamentals of general linear acoustics,
% but at low frequencies the difference is small
tao = t60/(6*log(10));
end

