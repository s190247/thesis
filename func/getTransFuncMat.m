function [H, H0] = getTransFuncMat(speakerErrors, roomTempErrors, mc_it, random_seed_speaker, random_seed_room_temp)
%GETTRANSFUNCMAT Summary of this function goes here
%   Detailed explanation goes here
if nargin < 5
    random_seed_room_temp = 0;
end
if nargin < 4
    random_seed_speaker = 1;
end
global c rho
rho = 1.204; %Density air
f = 1:1:300;
[dim, locs, locm] = setup_locations();
t60 = 0.6;
Tmin_Room = 293;
Tmax_Room = 299;

T0 = 296;
c = speedofsound(T0);
Hr0 = transFuncMat(locs, locm, dim, f, t60);
hs0 = speakerTF(repmat(23,size(locs,2)), f);
H0 = Hr0.*repmat(reshape(hs0, [1,size(locs,2),length(f)]), [size(locm,2),1,1]);

if nargin < 3
    mc_it = 200;
end

%% Load file if available

if speakerErrors && roomTempErrors
    filename = fullfile('data', strcat('H',string(mc_it),'SR','.mat'));
elseif speakerErrors && ~roomTempErrors
    filename = fullfile('data', strcat('H',string(mc_it),'S','.mat'));
elseif ~speakerErrors && roomTempErrors
    filename = fullfile('data', strcat('H',string(mc_it),'R','.mat'));
elseif ~speakerErrors && ~roomTempErrors
    filename = fullfile('data', strcat('H',string(mc_it),'.mat'));
end
    
if isfile(filename)
    load(filename, 'H');
else
    H = zeros(mc_it, size(locm,2), size(locs,2), length(f));
    if roomTempErrors
        T_vecRoom = random_temp(Tmin_Room, Tmax_Room, mc_it, random_seed_room_temp);
    else
        Hr = Hr0;
    end
    if speakerErrors
        T_vecCoil = random_tempCoil(mc_it, random_seed_speaker);
    else
        hs = hs0;
    end
    for it = 1:mc_it
        if roomTempErrors
            c = speedofsound(T_vecRoom(it));
            Hr = transFuncMat(locs, locm, dim, f, t60);
        end
        if speakerErrors
            hs = speakerTF(T_vecCoil(it,:), f);
        end
        H(it, :, :, :) = Hr.*repmat(reshape(hs, [1,size(locs,2),length(f)]), [size(locm,2),1,1]);
        disp(it);
    end
    save(filename,'H');
end
end

