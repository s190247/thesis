function [m_ac, std_ac, m_rep_err, std_rep_err, m_SPL, std_SPL] = runSim(Hsim, H0, regularization, mc_it, param)
%RUNSIM Summary of this function goes here
%   Detailed explanation goes here
if nargin <4
    mc_it = size(Hsim, 1);
end
if nargin <3
    regularization = 'off';
end

switch regularization
    case 'off'
        w = pmFilterRef(H0);
    case 'tikhonov'
        if nargin < 5
            param = 'eigen';
        end
        w = pmFilterTikhonov(H0, param);
    case 'tls'
        if nargin < 5
            param = 0;
        end
        w = pmFilterTLS(H0, param);
    case 'resample_temp'
        T0 = 296; Tmin = 293; Tmax = 299;
        T_vecRoom = random_temp(Tmin,Tmax, mc_it);
        [dim, locs, locm] = setup_locations();
        t60=0.6;
        f = 1:305;
        Hr0 = transFuncMat(locs, locm, dim, f, t60);
        q0 = speakerTF(repmat(20,size(locs,2)), f);
        H0Temp = Hr0.*repmat(reshape(q0, [1,size(locs,2),length(f)]), [size(locm,2),1,1]);
        w0 = pmFilterRef(H0Temp);
    case {'resample_temp_rr','resample_temp_error'}
        T0 = 296; Tmin = 293; Tmax = 299;
        T_vecRoom = random_temp(Tmin,Tmax, mc_it);
        [dim, locs, locm] = setup_locations();
        t60=0.6;
        f = 1:305;
        Hr0 = transFuncMat(locs, locm, dim, f, t60);
        q0 = speakerTF(repmat(20,size(locs,2)), f);
        H0Temp = Hr0.*repmat(reshape(q0, [1,size(locs,2),length(f)]), [size(locm,2),1,1]);
        wrr = pmFilterTikhonov(H0Temp, param);
    case 'pmo'
        Hpmo = getTransFuncMat(param(1),param(2), 150, 12,26);
        w = pmFilterStoch(Hpmo, H0);
    case 'pmoA'
        Hpmo = getTransFuncMat(1,1,param,15,2);
        w = pmFilterStoch(Hpmo, H0);
    case 'pmoZhu'
       w = pmFilterPMONoise(H0, param);

    case 'no_filter'
        w = zeros(8,300);
        w(7,:)=1;
    case 'acc'
        w = accFilterRef(H0);
        
end

ac = zeros(size(Hsim,1), size(Hsim,4));
rep_err = zeros(size(Hsim,1), size(Hsim,4));
SPL = zeros(size(Hsim,1), size(Hsim,2), size(Hsim,4)); 
for it = 1:mc_it
    switch regularization
        case 'optimal'
            w = pmFilterRef(squeeze(Hsim(it,:,:,:)));
        case 'resample_temp'
            T = T_vecRoom(it);
            w = resample_filter(w0, T0, T);
        case 'resample_temp_rr'
            T = T_vecRoom(it);
            w = resample_filter(wrr, T0, T);
        case 'resample_temp_error'
            T = T_vecRoom(it)+randn(1);
            w = resample_filter(wrr, T0, T);
    end
    ac(it,:) = ac_fromHw(squeeze(Hsim(it,:,:,:)),w);
    if nargout > 2 
%         rep_err(it,:) = repErr_fromHw(squeeze(Hsim(it,:,:,:)),w);
    end
    if nargout > 4
        SPL(it, :, :) = 20*log10(abs(p_fromHw(squeeze(Hsim(it,:,:,:)),w) / 2e-5));
    end
end

m_ac = mean(ac,1);
std_ac = std(ac,1);
if nargout > 2
    m_rep_err = mean(rep_err,1);
    std_rep_err = std(rep_err,1);
end
if nargout > 4
    m_SPL = squeeze(mean(SPL));
    std_SPL = squeeze(std(SPL));
end
end

