function [p, pB, pD] = p_fromHw(H,w)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
p = zeros(size(H,1),size(H,3));
for fit = 1:size(H,3)
    p(:,fit) = H(:,:,fit)*w(:,fit);
end
if nargout >2
    pB = sqrt(sum(abs(p(1:size(p,1)/2,:)).^2));
    pD = sqrt(sum(abs(p(size(p,1)/2:end,:)).^2));
end

end

