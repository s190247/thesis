function table_string = ac2latextable(m_ac, std_ac, speakerErrors, roomTempErrors, filter_name)
% Produces latex table from acoustic contrast 

freq_boundaries = [19, 60, 150, 300];
if speakerErrors && roomTempErrors
    condition = 'SR';
elseif speakerErrors && ~roomTempErrors
    condition = 'S';
elseif ~speakerErrors && roomTempErrors
    condition = 'R';
elseif ~speakerErrors && ~roomTempErrors
    condition = '0';
end

condition_string = strcat('$\bm{H}_{', condition, '}$');
filter_string = strcat('$\bm{w}_{', filter_name, '}$');
table_string = strcat(condition_string, '&', filter_string);
for it = 2:length(freq_boundaries)
    mean_m_ac = round(mean(m_ac(freq_boundaries(it-1)+1:freq_boundaries(it))),1);
    mean_std_ac = round(mean(std_ac(freq_boundaries(it-1)+1:freq_boundaries(it))),1);
    table_string = strcat(table_string, '& $', string(mean_m_ac), '\pm ', string(mean_std_ac),'$');
end
table_string = strcat(table_string, '\\ \hline');
end

