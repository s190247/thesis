addpath(genpath(pwd));

%% Initial
global c rho
rho = 1.204; %Density air
f = 1:1:300;
[dim, locs, locm] = setup_locations();
locm = locm(:,50);
locs = locs(:,1);
t60 = 0.6;
Tmin_Room = 293;
Tmax_Room = 299;

T0 = 296;
c = speedofsound(T0);
Hr0 = transFuncMat(locs, locm, dim, f, t60);
q0 = speakerTF(repmat(20,size(locs,2)), f);

mc_it = 200;
%% Room Deviations
T_vecRoom = random_temp(Tmin_Room, Tmax_Room, mc_it);
Hr = zeros(mc_it, size(locm,2), size(locs,2), length(f));
for it = 1:mc_it  
    c = speedofsound(T_vecRoom(it));
    Hr(it,:,:,:) = transFuncMat(locs, locm, dim, f, t60);
    disp(it);
end
%% Plot Room Deviations
figure;
colors = get_RGBColorCodes({'blue', 'red'});
m_Hr = squeeze(mean(10*log10(abs(Hr)),1));
max_Hr = max(squeeze(10*log10(abs(Hr))));
min_Hr = min(squeeze(10*log10(abs(Hr))));
std_Hr = squeeze(std(10*log10(abs(Hr)),1));
plot(m_Hr, 'Color',colors(:,1),'LineStyle','-','LineWidth',2, 'DisplayName', 'Mean $h_r$'); hold on;
plot(m_Hr-std_Hr,  'Color','k','LineStyle','-','LineWidth',.1,'DisplayName', 'Std $h_r$');
plot(m_Hr+std_Hr,  'Color','k','LineStyle','-','LineWidth',.1, 'HandleVisibility', 'off');
% plot(max_Hr, 'Color','k','LineStyle','-','LineWidth',1, 'HandleVisibility', 'off');
% plot(min_Hr, 'Color','k','LineStyle','-','LineWidth',1, 'HandleVisibility', 'off');
grid on;xlim([20,300]); set(gca, 'XScale', 'log'); 
xticks([20,50,100,200,300]); 
legend('Interpreter','latex','Location','SouthWest');
hold off
setSize([400 280]);
xlabel('Frequency [Hz]');
ylabel('Magnitude [dB]');
setFontSize([11;11;11;11]);
print(strcat('plotting',filesep,'plots',filesep,'roomDeviation'),'-depsc');
%% Speaker Deviations
T_vecCoil = random_tempCoil(mc_it, 1);
q = zeros(mc_it, 8, length(f));
for it = 1:mc_it
    q(it,:,:) = speakerTF(T_vecCoil(it,:), f);
    disp(it);
end
q = reshape(q, [size(q,1)*size(q,2), size(q,3)]);
m_q = squeeze(mean(10*log10(abs(q)),1));
std_q = squeeze(std(10*log10(abs(q)),1));
plot(m_q, 'Color',colors(:,2),'LineStyle','-','LineWidth',2, 'DisplayName', 'Mean $q_l$'); hold on; 
plot(m_q-std_q, 'Color','k', 'LineStyle',':','LineWidth',.1, 'DisplayName', 'Std $q_l$'); 
plot(m_q+std_q,  'Color','k','LineStyle',':','LineWidth',.1, 'HandleVisibility','off');
grid on;xlim([20,300]); set(gca, 'XScale', 'log'); 
xticks([20,50,100,200,300]); 
legend('Interpreter','latex');
hold off
setSize([400 280]);
setFontSize([11;11;11;11]);
xlabel('Frequency [Hz]');
ylabel('Magnitude [dB]');
print(strcat('plotting',filesep,'plots',filesep,'speakerDeviations'),'-depsc');

%% Plot standard deviation only
figure;
plot(std_q, 'Color','k','LineStyle','-','LineWidth',1); 
grid on;xlim([20,300]); set(gca, 'XScale', 'log'); xticks([20,50,100,200,300]); 
ylim([0 1.5])
setSize([400 280]);
setFontSize([11;11;11;11]);
xlabel('Frequency [Hz]');
ylabel('Magnitude [dB]');
legend('off');
print(strcat('plotting',filesep,'plots',filesep,'speakerMagStd'),'-depsc');
%% Plot Speaker Angle Deviations
figure;
m_angleq = mean(angle(q));
std_angleq = std(angle(q));
plot(rad2deg(std_angleq), 'Color','k','LineStyle','-','LineWidth',1); 
grid on;xlim([20,300]); set(gca, 'XScale', 'log'); xticks([20,50,100,200,300]); 
ylim([0 15])
setSize([400 280]);
setFontSize([11;11;11;11]);
xlabel('Frequency [Hz]');
ylabel('Phase [ $^{\circ}$]');
legend('off');
print(strcat('plotting',filesep,'plots',filesep,'speakerPhaseStd'),'-depsc');

%% Plot Voice Coil Histogram

[N,X]=hist(T_vecCoil(:), 22);
bar(X,N/length(T_vecCoil(:)),1, 'FaceColor',colors(:,2));
% histogram(T_vecCoil(:), 'Normalization', 'probability', 'FaceColor',colors(:,2));

xlabel('Voice Coil Temperature [$^{\circ}$C]');
ylabel('Probability');
setSize([400 280]);
grid on;
setFontSize([11;11;11;11]);
xlim([20,80]); xticks(30:10:90);
legend('off');
print(strcat('plotting',filesep,'plots',filesep,'VoiceCoilHist'),'-depsc');

%% Plot Room Standard Deviation
[dim, locs, locm] = setup_locations();
T_vecRoom = random_temp(Tmin_Room, Tmax_Room, mc_it);
Hr = zeros(mc_it, size(locm,2), size(locs,2), length(f));
for it = 1:mc_it  
    c = speedofsound(T_vecRoom(it));
    Hr(it,:,:,:) = transFuncMat(locs, locm, dim, f, t60);
    disp(it);
end
std_Hrall = squeeze(mean(mean(std(10*log10(abs(Hr)),1))));
plot(std_Hrall, 'Color','k','LineStyle','-','LineWidth',1, 'DisplayName', 'Std $H_{r,ml}$'); hold on; 
grid on;xlim([20,300]); set(gca, 'XScale', 'log'); 
xticks([20,50,100,200,300]); 
legend('Interpreter','latex');
hold off
setSize([400 280]);
setFontSize([11;11;11;11]);
xlabel('Frequency [Hz]');
ylabel('Magnitude [dB]');
legend('off');
print(strcat('plotting',filesep,'plots',filesep,'roomStd'),'-depsc');

%% Angle deviations
Hr_angle_unwrapped = rad2deg(unwrap(angle(Hr),[],1));
figure;
std_Hr_angle_unwrapped = squeeze((mean(mean(std(Hr_angle_unwrapped)))));
plot(std_Hr_angle_unwrapped,'k', 'LineWidth',1);
grid on;xlim([20,300]); set(gca, 'XScale', 'log'); xticks([20,50,100,200,300]); 

setSize([400 280]);
setFontSize([11;11;11;11]);
xlabel('Frequency [Hz]');
ylabel('Phase [ $^{\circ}$]');
legend('off');
print(strcat('plotting',filesep,'plots',filesep,'roomPhaseStd'),'-depsc');

%% Plot Room Temp Histogram
figure;
[N,X]=hist(T_vecRoom(:)-273, 10);
bar(X,N/mc_it,1, 'FaceColor',colors(:,1));
xlabel('Room Temperature [$^{\circ}$C]');
ylabel('Probability');
setSize([400 280]);
grid on;
setFontSize([11;11;11;11]);
% xlim([22,30]); %xticks(30:10:90);
legend('off');
print(strcat('plotting',filesep,'plots',filesep,'RoomHist'),'-depsc');
