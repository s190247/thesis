addpath(genpath(pwd));

[~, H0] = getTransFuncMat(false, false, 0);

w0 = pmFilterRef(H0);
wrr = pmFilterTikhonov(H0, 1e-3);
wtls = pmFilterTLS(H0);

Hpmo = getTransFuncMat(true,true, 150, 12,26);
wpmo = pmFilterStoch(Hpmo, H0);
%% Plot bright zone sound pressure
[~,pB0,pD0] = p_fromHw(H0,w0);
[~,pBrr,pDrr]=p_fromHw(H0,wrr);
[~,pBtls,pDtls]=p_fromHw(H0,wtls);

figure(1);
plot(20*log10(abs([pB0; pBrr; pBtls])'/2e-5))
hold on
plot(20*log10(abs([pD0; pDrr; pDtls])'/2e-5))
set(gca,'XScale','log');xlim([20,300]);xticks([20,50,100,200,300]);grid on;
%% Plot AC
ac0 = ac_fromHw(H0,w0);
acrr = ac_fromHw(H0,wrr);
actls = ac_fromHw(H0,wtls);

figure(2);
plot([ac0;acrr;actls]')
set(gca,'XScale','log');xlim([20,300]);xticks([20,50,100,200,300]);grid on;

%% Plot Array Effort
eff0 = array_effort(H0,w0);
effrr= array_effort(H0,wrr);
efftls = array_effort(H0,wtls);
effpmo = array_effort(H0,wpmo);
colors = [zeros(3,1), get_RGBColorCodes({'grey','orange','lavender'})];
figure(3);
plot(eff0, 'Color', colors(:,1), 'DisplayName', '{\boldmath${w}$}$_{0}$', 'LineWidth',2); hold on;
plot(effrr, 'Color', colors(:,2), 'DisplayName', '{\boldmath${w}$}$_{RR}$', 'LineWidth',1);
plot(efftls, 'Color',colors(:,3), 'DisplayName', '{\boldmath${w}$}$_{TLS}$', 'LineWidth',1);
plot(effpmo, 'Color', colors(:,4), 'DisplayName', '{\boldmath${w}$}$_{PMO,SR}$', 'LineWidth',1);
set(gca,'XScale','log');xlim([20,300]);xticks([20,50,100,200,300]);grid on;
setSize([800, 280]); setFontSize([11,11,11,11]);
xlabel('Frequency [Hz]'); ylabel('Effort [dB]');
hold off;
legend('NumColumns',2)
ylim([-5, 30])
print(strcat('plotting',filesep,'plots',filesep,'Effort'),'-depsc');



