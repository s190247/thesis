addpath(genpath(pwd));


speakerErrors_condition = [false, true,  true, false];
roomTempErrors_condition = [false, true,  false, true];
colors = get_RGBColorCodes({'olive','red','blue'});
colors = [0,0,0; colors']';
names = {'{\boldmath${H}$}$_{0}${\boldmath${w}$}$_{0}$', ...
         '{\boldmath${H}$}$_{SR}${\boldmath${w}$}$_{0}$', ...
         '{\boldmath${H}$}$_{S}${\boldmath${w}$}$_{0}$', ...
         '{\boldmath${H}$}$_{R}${\boldmath${w}$}$_{0}$'};
LineStyle = {'-','-','-','-'};
LineWidth = [2,2,1,1];
figure;
for it = 3
    speakerErrors = speakerErrors_condition(it);
    roomTempErrors = roomTempErrors_condition(it);
    [Hsim, H0] = getTransFuncMat(speakerErrors, roomTempErrors, 200);
    [m_ac, std_ac,rE] = runSim(Hsim, H0, 'off');
    table_string = ac2latextable(m_ac, std_ac, speakerErrors, roomTempErrors, '0');
    disp(table_string);
    %%
    plot(m_ac,'Color', colors(:,it), 'DisplayName', char(names(it)), 'LineStyle',char(LineStyle(it)),'LineWidth',LineWidth(it));
    hold on;
    plot(m_ac-std_ac,'Color', colors(:,it), 'HandleVisibility','off', 'LineStyle',':');
    plot(m_ac+std_ac,'Color', colors(:,it), 'HandleVisibility','off', 'LineStyle',':');
end

grid on; xlim([20,300]); set(gca, 'XScale', 'log'); 
xticks([20,50,100,200,300]); 
legend('NumColumns',2, 'Location', 'SouthWest');
xlabel('Frequency [Hz]');
ylabel('Acoustic Contrast [dB]');
setSize([800 280]);
setFontSize([11;11;11;11]);
hold off
print(strcat('plotting',filesep,'plots',filesep,'ACSpeakerRoom0'),'-depsc');

